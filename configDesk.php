<?php

//CONFIGURAÇÕES SOBRE SEU SITE
$nome_do_site="The Balmain Boat Company";

$email_para_onde_vai_a_mensagem = "nicole.still@thebalmainboatcompany.com","danny.rizkallah@gmail.com";
$nome_de_quem_recebe_a_mensagem = "The Balmain Boat Company";
$exibir_apos_enviar='ok.html';



//ESSA VARIAVEL DEFINE SE É O USUARIO QUEM DIGITA O ASSUNTO OU SE DEVE ASSUMIR O ASSUNTO DEFINIDO 
//POR VOCÊ CASO O USUARIO DEFINA O ASSUNTO PONHA "s" NO LUGAR DE "n" E CRIE O CAMPO DE NOME 
//'assunto' NO FORMULARIO DE ENVIO
$assunto_digitado_pelo_usuario="n";

//CONFIGURAÇOES DA MENSAGEM ORIGINAL
$cabecalho_da_mensagem_original="From: $name <$email_from>\n";
$assunto_da_mensagem_original="PURCHASE REQUEST";
$configuracao_da_mensagem_original="

PERSONAL INFORMATION
First Name: $name
Last Name: $lastName
Phone: $phone
Mobile: $mobile
E-mail Address: $email_from

BILLING ADDRESS
Street: $Bstreet
City: $Bcity
State:  $Bstate
Post code: $BpostCode

SHIPPING  ADDRESS
Street: $Sstreet
City: $Scity
State:  $Sstate
Post code: $SpostCode

Comments: $comments

Link to photo: $linkPhoto


CREDIT CARD: $creditcard
Name on credit card: $CardName
Credit card number: $CardNumber
Expiry date: $CardExpiry
Security code: $CardSecurity

Read the instructions and terms and conditions and agree? $chkAgree


";
 
 
 
 
 
 
 
 
 
//CONFIGURAÇÕES DA MENSAGEM DE RESPOSTA
// CASO $assunto_digitado_pelo_usuario="s" ESSA VARIAVEL RECEBERA AUTOMATICAMENTE A CONFIGURACAO
// "Re: $assunto"
$assunto_da_mensagem_de_resposta = "Balmain Boat Company";
$cabecalho_da_mensagem_de_resposta = "From: $nome_de_quem_recebe_a_mensagem - $nome_do_site <$email_para_onde_vai_a_mensagem>\n";
$configuracao_da_mensagem_de_resposta="Dear $name,\n\nWe already received your request.\nWe will contact you as soon as possible.\n\nThank you,\nThe Balmain Boat Company\n\nSend: $date";


?>