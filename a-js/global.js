function Browser() {

  var ua, s, i;

  this.isIE    = false; 
  this.isOP    = false;  
  this.isNS    = false;  
  this.version = null;

  ua = navigator.userAgent;

  s = "Opera";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isOP = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  s = "Netscape6/";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }



  s = "Gecko";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = 6.1;
    return;
  }

  s = "MSIE";
  if ((i = ua.indexOf(s))) {
    this.isIE = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }
}

var browser = new Browser();
var activeButton = null;

if (browser.isIE)
  document.onmousedown = pageMousedown;
else
  document.addEventListener("mousedown", pageMousedown, true);

function pageMousedown(event) {

  var el;
  if (activeButton == null)
    return;

if (browser.isIE)
    el = window.event.srcElement;
  else
    el = (event.target.tagName ? event.target : event.target.parentNode);

  if (el == activeButton)
    return;

  if (getContainerWith(el, "DIV", "menu") == null) {
    resetButton(activeButton);
    activeButton = null;
  }
}

function buttonclick(event, menuId) {

  var button;

  if (browser.isIE)
    button = window.event.srcElement;
  else
    button = event.currentTarget;

  button.blur();

  if (button.menu == null) {
    button.menu = document.getElementById(menuId);
    if (button.menu.isInitialized == null)
      menuInit(button.menu);
  }

  if (activeButton != null)
    resetButton(activeButton);

  if (button != activeButton) {
    depressButton(button);
    activeButton = button;
  }
  else
    activeButton = null;

  return false;
}

function buttonmouseover(event, menuId) {

  var button;

  if (browser.isIE)
    button = window.event.srcElement;
  else
    button = event.currentTarget;

  if (activeButton != null && activeButton != button)
    buttonclick(event, menuId);
}

function depressButton(button) {

  var x, y;

  button.className += " menuButtonActive";

  x = getPageOffsetLeft(button);
  y = getPageOffsetTop(button) + button.offsetHeight;

  if (browser.isIE) {
    x += button.offsetParent.clientLeft;
    y += button.offsetParent.clientTop;
  }

  button.menu.style.left = x + "px";
  button.menu.style.top  = y + "px";
  button.menu.style.visibility = "visible";

  if (button.menu.iframeEl != null)
  {
    button.menu.iframeEl.style.left = button.menu.style.left;
    button.menu.iframeEl.style.top  = button.menu.style.top;
    button.menu.iframeEl.style.width  = button.menu.offsetWidth + "px";
    button.menu.iframeEl.style.height = button.menu.offsetHeight + "px";
    button.menu.iframeEl.style.display = "";
  }
}

function resetButton(button) {

  removeClassName(button, "menuButtonActive");

  if (button.menu != null) {
    closeSubMenu(button.menu);
    button.menu.style.visibility = "hidden";

    if (button.menu.iframeEl != null)
      button.menu.iframeEl.style.display = "none";
  }
}

function menuMouseover(event) {

  var menu;

  if (browser.isIE)
    menu = getContainerWith(window.event.srcElement, "DIV", "menu");
  else
    menu = event.currentTarget;

  if (menu.activeItem != null)
    closeSubMenu(menu);
}

function menuItemMouseover(event, menuId) {

  var item, menu, x, y;

  if (browser.isIE)
    item = getContainerWith(window.event.srcElement, "A", "menuItem");
  else
    item = event.currentTarget;
  menu = getContainerWith(item, "DIV", "menu");

  if (menu.activeItem != null)
    closeSubMenu(menu);
  menu.activeItem = item;

  item.className += " menuItemHighlight";

  if (item.subMenu == null) {
    item.subMenu = document.getElementById(menuId);
    if (item.subMenu.isInitialized == null)
      menuInit(item.subMenu);
  }

  x = getPageOffsetLeft(item) + item.offsetWidth;
  y = getPageOffsetTop(item);

  var maxX, maxY;

  if (browser.isIE) {
    maxX = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft) +
      (document.documentElement.clientWidth != 0 ? document.documentElement.clientWidth : document.body.clientWidth);
    maxY = Math.max(document.documentElement.scrollTop, document.body.scrollTop) +
      (document.documentElement.clientHeight != 0 ? document.documentElement.clientHeight : document.body.clientHeight);
  }
  if (browser.isOP) {
    maxX = document.documentElement.scrollLeft + window.innerWidth;
    maxY = document.documentElement.scrollTop  + window.innerHeight;
  }
  if (browser.isNS) {
    maxX = window.scrollX + window.innerWidth;
    maxY = window.scrollY + window.innerHeight;
  }
  maxX -= item.subMenu.offsetWidth;
  maxY -= item.subMenu.offsetHeight;

  if (x > maxX)
    x = Math.max(0, x - item.offsetWidth - item.subMenu.offsetWidth
      + (menu.offsetWidth - item.offsetWidth));
  y = Math.max(0, Math.min(y, maxY));

  item.subMenu.style.left       = x + "px";
  item.subMenu.style.top        = y + "px";
  item.subMenu.style.visibility = "visible";

  if (item.subMenu.iframeEl != null)
  {
    item.subMenu.iframeEl.style.left    = item.subMenu.style.left;
    item.subMenu.iframeEl.style.top     = item.subMenu.style.top;
    item.subMenu.iframeEl.style.width   = item.subMenu.offsetWidth + "px";
    item.subMenu.iframeEl.style.height  = item.subMenu.offsetHeight + "px";
    item.subMenu.iframeEl.style.display = "";
  }

  if (browser.isIE)
    window.event.cancelBubble = true;
  else
    event.stopPropagation();
}

function closeSubMenu(menu) {

  if (menu == null || menu.activeItem == null)
    return;

  if (menu.activeItem.subMenu != null) {
    closeSubMenu(menu.activeItem.subMenu);

    menu.activeItem.subMenu.style.visibility = "hidden";

    if (menu.activeItem.subMenu.iframeEl != null)
      menu.activeItem.subMenu.iframeEl.style.display = "none";

    menu.activeItem.subMenu = null;
  }

  removeClassName(menu.activeItem, "menuItemHighlight");
  menu.activeItem = null;
}

function menuInit(menu) {

  var itemList, spanList;
  var textEl, arrowEl;
  var itemWidth;
  var w, dw;
  var i, j;



  if (browser.isIE) {
    menu.style.lineHeight = "2.5ex";
    spanList = menu.getElementsByTagName("SPAN");
    for (i = 0; i < spanList.length; i++)
      if (hasClassName(spanList[i], "menuItemArrow")) {
        spanList[i].style.fontFamily = "Webdings";
        spanList[i].firstChild.nodeValue = "4";
      }
  }

  itemList = menu.getElementsByTagName("A");
  if (itemList.length > 0)
    itemWidth = itemList[0].offsetWidth;
  else
    return;

  for (i = 0; i < itemList.length; i++) {
    spanList = itemList[i].getElementsByTagName("SPAN");
    textEl  = null;
    arrowEl = null;
    for (j = 0; j < spanList.length; j++) {
      if (hasClassName(spanList[j], "menuItemText"))
        textEl = spanList[j];
      if (hasClassName(spanList[j], "menuItemArrow")) {
        arrowEl = spanList[j];
      }
    }
    if (textEl != null && arrowEl != null) {
      textEl.style.paddingRight = (itemWidth 
        - (textEl.offsetWidth + arrowEl.offsetWidth)) + "px";

      if (browser.isOP)
        arrowEl.style.marginRight = "0px";
    }
  }

  if (browser.isIE) {
    w = itemList[0].offsetWidth;
    itemList[0].style.width = w + "px";
    dw = itemList[0].offsetWidth - w;
    w -= dw;
    itemList[0].style.width = w + "px";
  }

  if (browser.isIE) {
    var iframeEl = document.createElement("IFRAME");
    iframeEl.frameBorder = 0;
    iframeEl.src = "javascript:false;";
    iframeEl.style.display = "none";
    iframeEl.style.position = "absolute";
    iframeEl.style.filter = "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)";
    menu.iframeEl = menu.parentNode.insertBefore(iframeEl, menu);
  }

  menu.isInitialized = true;
}

function getContainerWith(node, tagName, className) {
  while (node != null) {
    if (node.tagName != null && node.tagName == tagName &&
        hasClassName(node, className))
      return node;
    node = node.parentNode;
  }

  return node;
}

function hasClassName(el, name) {

  var i, list;

  list = el.className.split(" ");
  for (i = 0; i < list.length; i++)
    if (list[i] == name)
      return true;

  return false;
}

function removeClassName(el, name) {

  var i, curList, newList;

  if (el.className == null)
    return;

  newList = new Array();
  curList = el.className.split(" ");
  for (i = 0; i < curList.length; i++)
    if (curList[i] != name)
      newList.push(curList[i]);
  el.className = newList.join(" ");
}

function getPageOffsetLeft(el) {

  var x;

  x = el.offsetLeft;
  if (el.offsetParent != null)
    x += getPageOffsetLeft(el.offsetParent);

  return x;
}

function getPageOffsetTop(el) {

  var y;

  y = el.offsetTop;
  if (el.offsetParent != null)
    y += getPageOffsetTop(el.offsetParent);

  return y;
}

function page_switch($sitelink)
{
	document.formRegion.action = '/a-transfer.php';
	document.formRegion.submit();
}

function page_switch_regions()
{
	document.formRegions.action = '';
	document.formRegions.submit();

}

function archive(archiveYear)
{
	document.formRegions.archiveYear.value = archiveYear;
	document.formRegions.action = '';
	document.formRegions.submit();

}

function div_popup(showhide){
	if(showhide == "show"){
			document.getElementById('popupbox').style.visibility="visible"; 
			document.getElementById('popupbox').style.display = ""; 
	}else if(showhide == "hide"){
			document.getElementById('popupbox').style.visibility="hidden"; 
			document.getElementById('popupbox').style.display = "none"; 
	}
}

function div_popup_2(showhide){
	if(showhide == "show"){
			document.getElementById('popupbox_2').style.visibility="visible"; 
	}else if(showhide == "hide"){
			document.getElementById('popupbox_2').style.visibility="hidden"; 
	}
	
}
function div_jobs(showhide){
	if(showhide == "show"){
			document.getElementById('jobs').style.visibility="visible"; 

	}else if(showhide == "hide"){
			document.getElementById('jobs').style.visibility="hidden"; 
	}
}
function div_email_friend(showhide){
	if(showhide == "show"){
			document.getElementById('emails_friend').style.visibility="visible"; 
	}else if(showhide == "hide"){
			document.getElementById('emails_friend').style.visibility="hidden"; 
	}
}
function div_campagin(showhide){
	if(showhide == "show"){
			document.getElementById('campaign').style.visibility="visible"; 
	}else if(showhide == "hide"){
			document.getElementById('campaign').style.visibility="hidden"; 
	}
}


	function checkall()
	{
				var str;
				str = document.RegistrationForm.allE.checked;
				if(str == true) {
				document.RegistrationForm.largeE.checked = true;
				document.RegistrationForm.upperE.checked = true;
				document.RegistrationForm.lowerE.checked = true;
				document.RegistrationForm.smallE.checked = true;
				document.RegistrationForm.allE.checked = true;
								} 
				if(str == false) {
				document.RegistrationForm.largeE.checked = false;
				document.RegistrationForm.upperE.checked = false;
				document.RegistrationForm.lowerE.checked = false;
				document.RegistrationForm.smallE.checked = false;
				document.RegistrationForm.allE.checked = false;
				} 
	}

function div_video_clip(showhide){
	if(showhide == "show"){
			document.getElementById('video_clip').style.visibility="visible"; 
			document.getElementById('video_clip').style.display = ""; 
	}else if(showhide == "hide"){
			document.getElementById('video_clip').style.visibility="hidden"; 
			document.getElementById('video_clip').style.display = "none"; 
	}
}

function div_video_clip02(showhide){
	if(showhide == "show"){
			document.getElementById('video_clip02').style.visibility="visible"; 
			document.getElementById('video_clip02').style.display = ""; 
	}else if(showhide == "hide"){
			document.getElementById('video_clip02').style.visibility="hidden"; 
			document.getElementById('video_clip02').style.display = "none"; 
	}
}

function div_video_job(showhide){
	if(showhide == "show"){
			document.getElementById('video_job').style.visibility="visible"; 
			document.getElementById('video_job').style.display = ""; 
	}else if(showhide == "hide"){
			document.getElementById('video_job').style.visibility="hidden"; 
			document.getElementById('video_job').style.display = "none"; 
	}
}

