
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head> 
<link rel="stylesheet" type="text/css" href="style.css"> 
 
<script src="functions.js" language="javascript" type="text/javascript" > </script> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
 
<title>The Balmain Boat Company</title> 
 
<script type="text/javascript" src="Scripts/jquery-1.3.1.min.js"></script> 
<script type="text/javascript"> 
 
$(document).ready(function() {		
	
	//Execute the slideShow
	slideShow();
 
});
 
function slideShow() {
 
	//Set the opacity of all images to 0
	$('#banner a').css({opacity: 0.0});
	
	//Get the first image and display it (set it to full opacity)
	$('#banner a:first').css({opacity: 1.0});
	
	//Set the caption background to semi-transparent
	$('#banner .caption').css({opacity: 0.7});
 
	//Resize the width of the caption according to the image width
	$('#banner .caption').css({width: $('#banner a').find('img').css('width')});
	
	//Get the caption of the first image from REL attribute and display it
	$('#banner .content').html($('#banner a:first').find('img').attr('rel'))
	.animate({opacity: 0.7}, 400);
	
	//Call the gallery function to run the slideshow, 6000 = change to next image after 6 seconds
	setInterval('gallery()',6000);
	
}
 
function gallery() {
	
	//if no IMGs have the show class, grab the first image
	var current = ($('#banner a.show')?  $('#banner a.show') : $('#banner a:first'));
 
	//Get next image, if it reached the end of the slideshow, rotate it back to the first image
	var next = ((current.next().length) ? ((current.next().hasClass('caption'))? $('#banner a:first') :current.next()) : $('#banner a:first'));	
	
	//Get next image caption
	var caption = next.find('img').attr('rel');	
	
	//Set the fade in effect for the next image, show class has higher z-index
	next.css({opacity: 0.0})
	.addClass('show')
	.animate({opacity: 1.0}, 1000);
 
	//Hide the current image
	current.animate({opacity: 0.0}, 1000)
	.removeClass('show');
	
	//Set the opacity to 0 and height to 1px
	$('#banner .caption').animate({opacity: 0.0}, { queue:false, duration:0 }).animate({height: '1px'}, { queue:true, duration:300 });	
	
	//Animate the caption, opacity to 0.7 and heigth to 100px, a slide up effect
	$('#banner .caption').animate({opacity: 0.7},100 ).animate({height: '100px'},500 );
	
	//Display the content
	$('#banner .content').html(caption);
	
	
}
 
</script> 
 
<style type="text/css">
<!--
.style1 {
	font-size: 18px;
	font-weight: bold;
}
-->
</style>
</head> 
<body> 
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle"><table width="95%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="47" colspan="2" align="left"><span class="style1">Shipping Information</span></td>
      </tr>
      <tr>
        <td width="198" align="left" valign="top"><img src="images/Shipping2.jpg" /></td>
        <td width="452" align="left"><strong>In Australia</strong><br />
          All boats will be dispatched from our Sydney warehouse within 7-10 business days from receipt of payment. 
          After dispatch, shipping time to most capital cities is usually 7-10 business days.<br />
              <br />
          Shipping charges in Australia are based on order size and post code and average $125 AUD per order by courier.
          Dates are estimates only and we apologize in advance for any delays that may be experienced.<br />
              <br />
              <strong>Overseas</strong><br />
          All boats will be dispatched from our overseas suppliers within 14 business days from receipt of payment. 
          After dispatch, shipping time to most capital cities is usually 7-10 business days.<br />
              <br />
          Shipping charges oveseas are based on order size and post code.  Before confirming your order, we will confirming 
          shipping charges with you via email.  We estimate shipping charges of $125 USD per order.
          Dates are estimates only and we apologize in advance for any delays that may be experienced.<br />
              <br />
          To buy a BBCo Boat Kit <a href="directions.htm" target="_blank">click here</a> <br/></td>
      </tr>
    </table></td>
  </tr>
</table>
</body> 
</html> 