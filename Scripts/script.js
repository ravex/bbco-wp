// JavaScript Document

//Mensagem de carregando (DESIGNER, MEXA SÃ" AQUI!)
ajax_Carregando='<span class="carregando" >&nbsp;&nbsp;Loading...</span>';

//Nossa fila de conexÃµes
ajax_Fila=[];
//VariÃ¡vel que vai guardar a funÃ§Ã£o de retorno
ajax_retF=false;
//Nossas filas "auxiliares" de retornos (HTML e JSON)
filaHTML=[];
filaJSON=[];


//O velho cÃ³digo pra criar o objeto...
try{
	//Tenta do jeito certo
	xmlhttp=new XMLHttpRequest();
}catch(e){
	try{
		//Tenta do jeito "IE6"
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(E){
		try{
			//Tenta do jeito "IE5"
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(ee){
			//Desiste
			xmlhttp = false;
		}
	}
}


/*
FunÃ§Ã£o "principal": pÃµe as coisas na fila e,
se a fila nÃ£o estiver sendo executada, chama
a doRealRequest().
*/
function doRequest(url,funcao_retorno,dados){
	//PÃµe na fila (por Ãºltimo)
	ajax_Fila.push([url,funcao_retorno,dados])
	//Se sÃ³ existir essa requisiÃ§Ã£o na fila, chama o doRealRequest
	if(ajax_Fila.length==1) doRealRequest();
}

/*
Pega a primeira requisiÃ§Ã£o da fila e executa,
dizendo pra chamar a ret_doRequest() quando
terminar.
*/
function doRealRequest(){
	//Pega as informaÃ§Ãµes da primeira requisiÃ§Ã£o da fila
	requisicao=ajax_Fila[0]
		url=requisicao[0]
		funcao_retorno=requisicao[1]
		dados=requisicao[2]

	//Se nÃ£o tem "dados", Ã© GET; se tem, Ã© POST.
	metodo="GET"
	if(dados) metodo="POST";

	//"Prepara" a requisiÃ§Ã£o
	xmlhttp.open(metodo,url,true)
	//Diz pra chamar a ret_doRequest() quando terminar
	xmlhttp.onreadystatechange=ret_doRequest
	//Se tem "dados", passa o cabeÃ§alho (pra envio via POST)
	if(dados) xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	//Faz a requisiÃ§Ã£o
	xmlhttp.send(dados)

}

/*
Ã‰ chamada quando a requisiÃ§Ã£o termina. Executa a
funÃ§Ã£o de retorno relativa a essa requisiÃ§Ã£o, e
remove a requisiÃ§Ã£o da fila. Depois, vÃª se ainda
hÃ¡ requisiÃ§Ãµes na fila e, se houver, chama a
doRealRequest().
*/
function ret_doRequest(){
	//Se a requisiÃ§Ã£o terminou
	if(xmlhttp.readyState==4){

		//Pega as informaÃ§Ãµes da primeira requisiÃ§Ã£o da fila
		requisicao=ajax_Fila[0]
			url=requisicao[0]
			funcao_retorno=requisicao[1]
			dados=requisicao[2]
		//Chama a funÃ§Ã£o de retorno relativa a essa requisiÃ§Ã£o,
		//  passando como parÃ¢metro o texto que veio do servidor
		funcao_retorno(xmlhttp.responseText)
		//Remove a primeira requisiÃ§Ã£o da fila
		ajax_Fila.shift()
		//Se ainda tem alguma coisa na fila, chama a 
		//  doRealRequest() (com o truquezinho dos 20 milisegundos)
		if(ajax_Fila.length>0) setTimeout("doRealRequest()",20);
	}
}

/////////////////////////

/*
PÃµe o id de um elemento HTML na filaHTML e cria
uma requisiÃ§Ã£o Ã  url dizendo pra, quando a
requisiÃ§Ã£o terminar, chamar a funÃ§Ã£o
ret_doHTML().
*/
function doHTML(url,idelemento,dados){
	//PÃµe o id no final da fila
	filaHTML.push(idelemento)
	//Escreve a mensagem de "carregando" no elemento
	document.getElementById(idelemento).innerHTML=ajax_Carregando
	//Cria a requisiÃ§Ã£o
	doRequest(url,ret_doHTML,dados)
}

/*
Recebe o texto que a doHTML() mandou buscar
no servidor. Joga esse texto no conteÃºdo do
primeiro elemento da filaHTML, e remove esse
elemento da fila.
*/
function ret_doHTML(responseText){
	//Pega o primeiro elemento da fila
	idelemento=filaHTML[0]
	//Escreve o texto (desencodado) nele
	document.getElementById(idelemento).innerHTML=unescape(responseText.replace(/\+/g," "))
	//Arranca o elemento da filaHTML
	filaHTML.shift()
}

/////////////////////////

/*
PÃµe uma funÃ§Ã£o de retorno na filaJSON e cria
uma requisiÃ§Ã£o Ã  url dizendo pra, quando a
requisiÃ§Ã£o terminar, chamar a funÃ§Ã£o
ret_doJSON().
*/
function doJSON(url,funcao_retorno,dados){
	//PÃµe a funÃ§Ã£o de retorno no final da fila
	filaJSON.push(funcao_retorno)
	//Cria a requisiÃ§Ã£o
	doRequest(url,ret_doJSON,dados);
}

/*
Recebe o javascript que a doJSON() mandou buscar
no servidor. "Evalua" esse texto e passa pra
funÃ§Ã£o de retorno, que era o primeiro elemento
da nossa filaJSON. E enfim, remove esse elemento
da fila.
*/
function ret_doJSON(responseText){
	//Pega a funÃ§Ã£o de retorno (primeiro elemento da nossa filaJSON)
	funcao_retorno=filaJSON[0];
	//"Evalua" o texto (javascript) que veio do servidor
	funcao_retorno(eval("["+responseText+"]")[0]);
	//Arranca o elemento da filaJSON
	filaJSON.shift();
}







////////////////////////////////////
//                                //
//   Presentinhos do Elcio: ;-)   //
//                                //
////////////////////////////////////

/*
doURLEncode
	Recebe um formulÃ¡rio ou um dicionÃ¡rio (Array nomeado, objeto,
	ou como vocÃª preferir chamar) e monta uma querystring
*/
function doURLEncode(d){
	var t="";
	if(d.innerHTML){
		var d2={};
		for(var i=0;i<d.elements.length;i++){
			var o=d.elements[i];
			if(o.name) d2[o.name]=o.value;
		}
		d=d2;
	}
	for(var i in d)
		t+=encodeURIComponent(i)+"="+encodeURIComponent(d[i])+"&";
	return t;
}
//Vamos precisar disso
Array.prototype.last=function(){
	return this[this.length-1];
};







/***********************************************
* Switch Menu script- by Martial B of http://getElementById.com/
* Modified by Dynamic Drive for format & NS4/IE4 compatibility
* Visit http://www.dynamicdrive.com/ for full source code
***********************************************/

var persistmenu="yes" //"yes" or "no". Make sure each SPAN content contains an incrementing ID starting at 1 (id="sub1", id="sub2", etc)
var persisttype="sitewide" //enter "sitewide" for menu to persist across site, "local" for this page only

if (document.getElementById){ //DynamicDrive.com change
document.write('<style type="text/css">\n')
document.write('.submenu{display: none;}\n')
document.write('</style>\n')
}

function SwitchMenu(obj){
	if(document.getElementById){
	var el = document.getElementById(obj);
	if (el==null)
	{
		var ar = document.getElementById("masterdiv").getElementsByTagName("span"); //DynamicDrive.com change
		for (var i=0; i<ar.length; i++){
			if (ar[i].className=="submenu") //DynamicDrive.com change
				ar[i].style.display = "none";
		}
	}
	else {
		var ar = document.getElementById("masterdiv").getElementsByTagName("span"); //DynamicDrive.com change
		if(el.style.display != "block"){ //DynamicDrive.com change
			for (var i=0; i<ar.length; i++){
				if (ar[i].className=="submenu") //DynamicDrive.com change
					ar[i].style.display = "none";
			}
			el.style.display = "block";
		}else{
			el.style.display = "none";
		}
	}
}
}

function get_cookie(Name) { 
var search = Name + "=";
var returnvalue = "";
if (document.cookie.length > 0) {
offset = document.cookie.indexOf(search);
if (offset != -1) { 
offset += search.length
end = document.cookie.indexOf(";", offset);
if (end == -1) end = document.cookie.length;
returnvalue=unescape(document.cookie.substring(offset, end))
}
}
return returnvalue;
}

function onloadfunction(){
if (persistmenu=="yes"){
var cookiename=(persisttype=="sitewide")? "switchmenu" : window.location.pathname
var cookievalue=get_cookie(cookiename)
if (cookievalue!="")
document.getElementById(cookievalue).style.display="block"
}
}

function savemenustate(){
var inc=1, blockid=""
while (document.getElementById("sub"+inc)){
if (document.getElementById("sub"+inc).style.display=="block"){
blockid="sub"+inc
break
}
inc++
}
var cookiename=(persisttype=="sitewide")? "switchmenu" : window.location.pathname
var cookievalue=(persisttype=="sitewide")? blockid+";path=/" : blockid
document.cookie=cookiename+"="+cookievalue
}

if (window.addEventListener)
window.addEventListener("load", onloadfunction, false)
else if (window.attachEvent)
window.attachEvent("onload", onloadfunction)
else if (document.getElementById)
window.onload=onloadfunction

if (persistmenu=="yes" && document.getElementById)
window.onunload=savemenustate


function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}