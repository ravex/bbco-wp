=== WP SlimStat Dashboard Widgets ===
Contributors: coolmann
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=BNJR5EZNY3W38
Tags: chart, analytics, visitors, users, spy, shortstat, tracking, reports, seo, referers, analyze, wassup, geolocation, online users, spider, tracker, pageviews, world map, stats, maxmind, flot, stalker, statistics, google+, monitor, seo
Requires at least: 3.1
Tested up to: 3.6
Stable tag: 3.1

== Description ==
Add-on for WP SlimStat to allow admins to monitor their visitors directly from their WordPress dashboard.

= Requirements =
* Wordpress 3.1 or higher
* [WP SlimStat 3.3+](http://wordpress.org/extend/plugins/wp-slimstat/)
* PHP 5.1 or higher
* MySQL 5.0.3 or higher

= Browser Compatibility =
WP SlimStat Dashboard Widgets uses the HTML5 Canvas element to display its charts. Unfortunately Internet Explorer 8 and older versions don't support this functionality, so you're encouraged to upgrade your browser.

== Installation ==

1. Go to Plugins > Add New
2. Search for WP SlimStat Dashboard Widgets
3. Click on Install Now under WP SlimStat Dashboard Widgets

== Screenshots ==

1. Mobile view, to keep an eye on your stats on the go

== Changelog ==

= 3.1 =
* Fixed: now compatible with the latest version of SlimStat

= 3.0.2 =
* Fixed: using the new libraries available in WP SlimStat 3.3 (thank you, pepe)

= 3.0.1 =
* Fixed: fatal error when WP SlimStat was deactivated (thank you, [R4C](http://wordpress.org/support/topic/fatal-error-cant-log-into-wp-dashboard))

= 3.0 =
* Initial release as a standalone plugin