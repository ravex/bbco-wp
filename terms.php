
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head> 
<link rel="stylesheet" type="text/css" href="style.css"> 
 
<script src="functions.js" language="javascript" type="text/javascript" > </script> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
 
<title>The Balmain Boat Company</title> 
 
<script type="text/javascript" src="Scripts/jquery-1.3.1.min.js"></script> 
<script type="text/javascript"> 
 
$(document).ready(function() {		
	
	//Execute the slideShow
	slideShow();
 
});
 
function slideShow() {
 
	//Set the opacity of all images to 0
	$('#banner a').css({opacity: 0.0});
	
	//Get the first image and display it (set it to full opacity)
	$('#banner a:first').css({opacity: 1.0});
	
	//Set the caption background to semi-transparent
	$('#banner .caption').css({opacity: 0.7});
 
	//Resize the width of the caption according to the image width
	$('#banner .caption').css({width: $('#banner a').find('img').css('width')});
	
	//Get the caption of the first image from REL attribute and display it
	$('#banner .content').html($('#banner a:first').find('img').attr('rel'))
	.animate({opacity: 0.7}, 400);
	
	//Call the gallery function to run the slideshow, 6000 = change to next image after 6 seconds
	setInterval('gallery()',6000);
	
}
 
function gallery() {
	
	//if no IMGs have the show class, grab the first image
	var current = ($('#banner a.show')?  $('#banner a.show') : $('#banner a:first'));
 
	//Get next image, if it reached the end of the slideshow, rotate it back to the first image
	var next = ((current.next().length) ? ((current.next().hasClass('caption'))? $('#banner a:first') :current.next()) : $('#banner a:first'));	
	
	//Get next image caption
	var caption = next.find('img').attr('rel');	
	
	//Set the fade in effect for the next image, show class has higher z-index
	next.css({opacity: 0.0})
	.addClass('show')
	.animate({opacity: 1.0}, 1000);
 
	//Hide the current image
	current.animate({opacity: 0.0}, 1000)
	.removeClass('show');
	
	//Set the opacity to 0 and height to 1px
	$('#banner .caption').animate({opacity: 0.0}, { queue:false, duration:0 }).animate({height: '1px'}, { queue:true, duration:300 });	
	
	//Animate the caption, opacity to 0.7 and heigth to 100px, a slide up effect
	$('#banner .caption').animate({opacity: 0.7},100 ).animate({height: '100px'},500 );
	
	//Display the content
	$('#banner .content').html(caption);
	
	
}
 
</script> 
 
<style type="text/css">
<!--
.style1 {
	font-size: 18px;
	font-weight: bold;
}
-->
</style>
</head> 
<body> 
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle"><table width="95%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="47" align="left"><span class="style1">Legal Terms and Conditions</span></td>
      </tr>
      <tr>
        <td align="left" valign="top"><font ><strong>Legal Disclaimer</strong><br/>
Please follow all instructions provided on both the pictorial boat assembly 
          instructions and on the Balmain Boat Company website (www.thebalmainboatcompany.com) 
          carefully and completely. <br/>
The Balmain Boat Company and its directors, officers and employees discharge 
          all liability for any loss or damage to personal property or injuries 
          sustained including death to you or any other third party caused during 
          the assembly process or while using the boat in any capacity. You operate 
          this boat at your own risk. If you are at all uncomfortable or inexperienced 
          in boat building, please consider asking a boat builder to assist you 
          ”Please be aware of all maritime safety rules in your state and country 
          including but not limited to the mandatory use and provision of a lifejacket 
          for all passengers when operating the boat”.<br/>
You must be over 18 years of age to assemble and operate this boat or 
          be under constant supervision by a parent and/or guardian.” These instructions 
          are copyright and include reference to trademarks including the trademark BALMAIN BOAT COMPANY™. All other rights are reserved.<br/>
Requests and inquiries concerning the assembly of this boat, copyright 
          and other rights should be addressed to: Balmain Boat Company, 
          4/84 Campbell Parade, BONDI BEACH NSW 2026, 
          For further information please visit our website (www.thebalmainboatcompany.com), 
          email (info@thebalmainboatcompany.com) or phone +6140181262<br/>
          <br/>
          <br/>
          <br/>
          <strong>Terms and Conditions</strong><br/>
In using this website you are deemed to have read and agreed to the 
          following terms and conditions:<br/>
          <br/>
The following terminology applies to these Terms and Conditions, Privacy 
          Statement and Disclaimer Notice and any or all Agreements: &quot;Client&quot;, 
          “You” and “Your” refers to you, the person accessing this website and 
          accepting the Company’s terms and conditions. &quot;The Company&quot;, “Ourselves”, 
          “We” and &quot;Us&quot;, refers to our Company. “Party”, “Parties”, or “Us”, refers 
          to both the Client and ourselves, or either the Client or ourselves. 
          All terms refer to the offer, acceptance and consideration of payment 
          necessary to undertake the process of our assistance to the Client in 
          the most appropriate manner, whether by formal meetings of a fixed duration, 
          or any other means, for the express purpose of meeting the Client’s 
          needs in respect of provision of the Company’s stated services/products, 
          in accordance with and subject to, prevailing Australian Law. Any use 
          of the above terminology or other words in the singular, plural, capitalisation 
          and/or he/she or they, are taken as interchangeable and therefore as 
          referring to same.<br/>
          <br/>
          <strong>Privacy Statement:</strong><br/>
We are committed to protecting your privacy. Authorized employees within the 
		  company on a need to know basis only use any information collected from individual customers. 
		  We constantly review our systems and data to ensure the best possible service to our customers. 
		  Parliament has created specific offences for unauthorised actions against computer systems and data. 
		  We will investigate any such actions with a view to prosecuting and/or taking civil proceedings to 
		  recover damages against those responsible.<br/>
		  <br/>
          <strong>Confidentiality</strong><br/>
We are registered under the Data Protection Act 1998 and as such, any information concerning the Client and 
		  their respective Client Records may be passed to third parties. However, Client records are regarded as confidential 
		  and therefore will not be divulged to any third party, other than [our manufacturer/supplier(s) and] if legally 
		  required to do so to the appropriate authorities. Clients have the right to request sight of, and copies of any and 
		  all Client Records we keep, on the proviso that we are given reasonable notice of such a request. Clients are requested 
		  to retain copies of any literature issued in relation to the provision of our services. Where appropriate, we shall 
		  issue Client’s with appropriate written information, handouts or copies of records as part of an agreed contract, 
		  for the benefit of both parties.<br/>
		  <br/>
We will not sell, share, or rent your personal information to any third party or use your e-mail address for unsolicited 
		  mail. Any emails sent by this Company will only be in connection with the provision of agreed services and products.<br/>
		  <br/>
          <strong>Disclaimer</strong><br/>
Exclusions and Limitations<br/>
The information on this web site is provided on an &quot;as is&quot; basis. To the fullest extent permitted by law, this Company:
		  excludes all representations and warranties relating to this website and its contents or which is or may be provided by 
		  any affiliates or any other third party, including in relation to any inaccuracies or omissions in this website and/or 
		  the Company’s literature; and excludes all liability for damages arising out of or in connection with your use of this 
		  website. This includes, without limitation, direct loss, loss of business or profits (whether or not the loss of such 
		  profits was foreseeable, arose in the normal course of things or you have advised this Company of the possibility of 
		  such potential loss), damage caused to your computer, computer software, systems and programs and the data thereon or 
		  any other direct or indirect, consequential and incidental damages. This Company does not however exclude liability 
		  for death or personal injury caused by its negligence. The above exclusions and limitations apply only to the extent 
		  permitted by law. None of your statutory rights as a consumer are affected.<br/>
		  <br/>
          <strong>Payment</strong><br/>
All major Credit/Debit Cards and Paypal are all acceptable methods of payment. Our Terms are payment in full before 
		  delivery. All goods remain the property of the Company until paid for in full.<br/>
		  <br/>
          <strong>Termination of Agreements and Refunds Policy</strong><br/>
Both the Client and ourselves have the right to terminate any Services Agreement for any reason, including the ending 
		  of services that are already underway. No refunds shall be offered, where a Service is deemed to have begun and is, 
		  for all intents and purposes, underway. <br/>
		  <br/>
          <strong>Log Files</strong><br/>
We use IP addresses to analyse trends, administer the site, track user’s movement, and gather broad demographic 
		  information for aggregate use. IP addresses are not linked to personally identifiable information. Additionally, for 
		  systems administration, detecting usage patterns and troubleshooting purposes, our web servers automatically log 
		  standard access information including browser type, access times/open mail, URL requested, and referral URL. 
		  This information is not shared with third parties and is used only within this Company on a need-to-know basis. 
		  Any individually identifiable information related to this data will never be used in any way different to that 
		  stated above without your explicit permission.<br/>
		  <br/>
Links from this website  We do not monitor or review the content of other party’s websites which are linked to 
		  from this website. Opinions expressed or material appearing on such websites are not necessarily shared or endorsed 
		  by us and should not be regarded as the publisher of such opinions or material. Please be aware that we are not 
		  responsible for the privacy practices, or content, of these sites. We encourage our users to be aware when they 
		  leave our site &amp; to read the privacy statements of these sites. You should evaluate the security and trustworthiness 
		  of any other site connected to this site or accessed through this site yourself, before disclosing any personal 
		  information to them. This Company will not accept any responsibility for any loss or damage in whatever manner, 
		  howsoever caused, resulting from your disclosure to third parties of personal information.<br/>
		  <br/>
          <strong>Copyright Notice</strong><br/>
Copyright and other relevant intellectual property rights exists on all text relating to the Company’s services and 
		  the full content of this website.<br/>
		  <br/>
          <strong>Communication</strong><br/>
We have several different e-mail addresses for different queries. These, &amp; other contact information, can be found 
		  on our Contact Us link on our website or via Company literature or via the Company’s stated telephone, facsimile 
		  or mobile telephone numbers.<br/>
		  <br/>
          <strong>Waiver</strong><br/>
Failure of either Party to insist upon strict performance of any provision of this or any Agreement or the failure of 
		  either Party to exercise any right or remedy to which it, he or they are entitled hereunder shall not constitute a 
		  waiver thereof and shall not cause a diminution of the obligations under this or any Agreement. No waiver of any of 
		  the provisions of this or any Agreement shall be effective unless it is expressly stated to be such and signed by both 
		  Parties.<br/>
		  <br/>
          <strong>Notification of Changes</strong><br/>
The Company reserves the right to change these conditions from time to time as it sees fit and your continued use of 
		  the site will signify your acceptance of any adjustment to these terms. If there are any changes to our privacy policy, 
		  we will announce that these changes have been made on our home page and on other key pages on our site. If there are 
		  any changes in how we use our site customers’ Personally Identifiable Information, notification by e-mail or postal 
		  mail will be made to those affected by this change. Any changes to our privacy policy will be posted on our web site 
		  30 days prior to these changes taking place. You are therefore advised to re-read this statement on a regular basis.<br/>
		  <br/>
These terms and conditions form part of the Agreement between the Client and ourselves. Your accessing of this website 
		  and/or undertaking of a booking or Agreement indicates your understanding, agreement to and acceptance, of the Disclaimer 
		  Notice and the full Terms and Conditions contained herein. Your statutory Consumer Rights are unaffected. <br/>
		  <br/>
© The Balmain Boat Company Pty Ltd 2011. All Rights Reserved </font><br/>
<br />
<br />
<br /></td>
        </tr>
    </table></td>
  </tr>
</table>
</body> 
</html> 