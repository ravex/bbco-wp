<style>
.emailtx{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.emailtx02 {
	padding-top: 3px;
	padding-right: 10px;
	padding-bottom: 3px;
	padding-left: 10px;
}
.emailtitle {
	padding-top: 3px;
	padding-right: 10px;
	padding-bottom: 3px;
	padding-left: 10px;
	font-weight: bold;
}
</style>
<table width='600' border='0' cellpadding='0' cellspacing='1' bgcolor='#FFFFFF' class='emailtx'>
  <tr>
    <td colspan='2' class='emailtitle'><b>CUSTOMER DETAILS</b></td>
  </tr>
  <tr>
    <td width='216' class='emailtx02'>First Name</td>
    <td width='384' class='emailtx02'>$fname</td>
  </tr>
  <tr>
    <td class='emailtx02'>Last Name</td>
    <td class='emailtx02'>$lname</td>
  </tr>
  <tr>
    <td class='emailtx02'>Phone</td>
    <td class='emailtx02'>$tel $tel02 $tel03</td>
  </tr>
  <tr>
    <td class='emailtx02'>Mobile</td>
    <td class='emailtx02'>$mobile $mobile02 $mobile03</td>
  </tr>
  <tr>
    <td class='emailtx02'>E-mail Address</td>
    <td class='emailtx02'>$email</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2' class='emailtitle'><b>BILLING ADDRESS</b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class='emailtx02'>Street No.: </td>
    <td class='emailtx02'>$bill_streetno</td>
  </tr>
  <tr>
    <td class='emailtx02'>Street Name: </td>
    <td class='emailtx02'>$bill_street</td>
  </tr>
  <tr>
    <td class='emailtx02'>City: </td>
    <td class='emailtx02'>$bill_city</td>
  </tr>
  <tr>
    <td class='emailtx02'>Apt No.: </td>
    <td class='emailtx02'>$bill_apt</td>
  </tr>
  <tr>
    <td class='emailtx02'>State: </td>
    <td class='emailtx02'>$bill_state</td>
  </tr>
  <tr>
    <td class='emailtx02'>Post code</td>
    <td class='emailtx02'>$bill_zip</td>
  </tr>
  <tr>
    <td class='emailtx02'>Country</td>
    <td class='emailtx02'>$bill_country</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2' class='emailtitle'><b>SHIPPING  ADDRESS</b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class='emailtx02'>Street No.: </td>
    <td class='emailtx02'>$ship_streetno</td>
  </tr>
  <tr>
    <td class='emailtx02'>Street Name: </td>
    <td class='emailtx02'>$ship_street</td>
  </tr>
  <tr>
    <td class='emailtx02'>Atp No. : </td>
    <td class='emailtx02'>$ship_apt</td>
  </tr>
  <tr>
    <td class='emailtx02'>City: </td>
    <td class='emailtx02'>$ship_city</td>
  </tr>
  <tr>
    <td class='emailtx02'>State: </td>
    <td class='emailtx02'>$ship_state</td>
  </tr>
  <tr>
    <td class='emailtx02'>Post code</td>
    <td class='emailtx02'>$ship_zip</td>
  </tr>
  <tr>
    <td class='emailtx02'>Country</td>
    <td class='emailtx02'>$ship_country</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2' class='emailtx02'><b>Comments</b>:</td>
  </tr>
  <tr>
    <td colspan='2' class='emailtx02'>$comments</td>
  </tr>
  <tr>
    <td class='emailtitle'>Photo:</td>
    <td class='emailtx02'><a href='http://www.wikwakdesigns.com/balmain/a-photos/$newfilename'>Download</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2' class='emailtitle'><b>PAYMENT INFORMATION</b> </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class='emailtx02'>Creditcard Type: </td>
    <td class='emailtx02'>$CardType</td>
  </tr>
  <tr>
    <td class='emailtx02'>Credit card number: </td>
    <td class='emailtx02'>$CardNumber</td>
  </tr>
  <tr>
    <td class='emailtx02'>Expiry date: </td>
    <td class='emailtx02'>$ExpMon / $ExpYear</td>
  </tr>
  <tr>
    <td class='emailtx02'>Security code:</td>
    <td class='emailtx02'>$CardSecurity</td>
  </tr>
  <tr>
    <td class='emailtx02'>Name on credit card: </td>
    <td class='emailtx02'>$cardname</td>
  </tr>
</table>
