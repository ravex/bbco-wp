<link rel="stylesheet" type="text/css" href="a-css/global.css">
<link rel="stylesheet" type="text/css" href="../a-css/global.css">
<style>
.errors{
	color: #FF0000;
}
</style>
<?
//include("a-mysql/conn.php");
$error_open  = "<span class='errors'>";
$error_end = "</span>";

if($_POST['isSubmit'])
{
	foreach($_POST as $key => $value) { $$key = strip_tags($value); }
	extract($_POST);
	// general information
	if(!$fname)
	{
 		$error = 1;
		$ERRORS['fname'] = 1;
	}

	if(!$lname)
	{
 		$error = 1;
		$ERRORS['lname'] = 1;
	}

	if(!$tel || !$tel03 || !$tel03)
	{
 		$error = 1;
		$ERRORS['tel'] = 1;
	}

	if(!$email)
	{
 		$error = 1;
		$ERRORS['email'] = 1;
	}

	if(!$email02)
	{
 		$error = 1;
		$ERRORS['email02'] = 1;
	}

	if(!valid_email($email02))
	{
 		$error = 1;
		$ERRORS['email02'] = 1;
	}

	if($email != $email02)
	{
 		$error = 1;
		$ERRORS['email02'] = 1;
	}

	if(!valid_email($email))
	{
 		$error = 1;
		$ERRORS['email'] = 1;
	}
	// billing inforamtion
	if(!$bill_streetno)
	{
 		$error = 1;
		$ERRORS['bill_streetno'] = 1;
	}

	if(!$bill_street)
	{
 		$error = 1;
		$ERRORS['bill_street'] = 1;
	}


	if(!$bill_city)
	{
 		$error = 1;
		$ERRORS['bill_city'] = 1;
	}

	if(!$bill_state)
	{
 		$error = 1;
		$ERRORS['bill_state'] = 1;
	}

	if(!$bill_zip)
	{
 		$error = 1;
		$ERRORS['bill_zip'] = 1;
	}
	
	if(!$bill_country)
	{
 		$error = 1;
		$ERRORS['bill_country'] = 1;
	}
	// shipping inforamtion
	if(!$ship_streetno)
	{
 		$error = 1;
		$ERRORS['ship_streetno'] = 1;
	}

	if(!$ship_street)
	{
 		$error = 1;
		$ERRORS['ship_street'] = 1;
	}

	if(!$ship_city)
	{
 		$error = 1;
		$ERRORS['ship_city'] = 1;
	}

	if(!$ship_state)
	{
 		$error = 1;
		$ERRORS['ship_state'] = 1;
	}

	if(!$ship_zip)
	{
 		$error = 1;
		$ERRORS['ship_zip'] = 1;
	}

	if(!$ship_country)
	{
 		$error = 1;
		$ERRORS['ship_country'] = 1;
	}

	if($chkAgree != 'Yes')
	{
 		$error = 1;
		$ERRORS['chkAgree'] = 1;
	}

	$allowed   = array("GIF","gif","JPG","jpg","JPEG","jpeg");
	$filename = $_FILES['photo']['tmp_name'];
	$ext = strtolower(end(explode('.', $_FILES['photo']['name'])));
		
	if(is_uploaded_file($filename)){
		if(in_array($ext, $allowed)){
		
		} else {
 		$error = 1;
		$ERRORS['photo'] = 1;
		
		}
	} 
//======================= validation ================================================

			$fname = eregi_replace("'","\'",$fname);
			$lname = eregi_replace("'","\'",$lname);
			$tel = eregi_replace("'","\'",$tel);
			$mobile = eregi_replace("'","\'",$mobile);
			$email = eregi_replace("'","\'",$email);
			
			$bill_street = eregi_replace("'","\'",$bill_street);
			$bill_city = eregi_replace("'","\'",$bill_city);
			$bill_state = eregi_replace("'","\'",$bill_state);
			$bill_zip = eregi_replace("'","\'",$bill_zip);
			
			$ship_street = eregi_replace("'","\'",$ship_street);
			$ship_city = eregi_replace("'","\'",$ship_city);
			$ship_state = eregi_replace("'","\'",$ship_state);
			$ship_zip = eregi_replace("'","\'",$ship_zip);
			
//=====================================================================================

if($ERRORS == "")
{
	$submit_status = 1; 

/*  upload file */
	$filename = $_FILES['photo']['tmp_name'];
	$ext = strtolower(end(explode('.', $_FILES['photo']['name'])));	   
	//if(is_uploaded_file($filename) && in_array($ext, $allowed)){
	if(is_uploaded_file($filename)){
			$fileName = $_FILES['photo']['name'];
			$ext = substr($fileName, strrpos($fileName, '.') + 1);
			$newfilename = "photo-".date(YmdHis).".".$ext;
			copy($_FILES['photo']['tmp_name'],"a-photos/".$newfilename);
	}


			
/* send emial */			
$message ="
<style>
.emailtx{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.emailtx02 {
	padding-top: 3px;
	padding-right: 10px;
	padding-bottom: 3px;
	padding-left: 10px;
}
.emailtitle {
	padding-top: 3px;
	padding-right: 10px;
	padding-bottom: 3px;
	padding-left: 10px;
	font-weight: bold;
}
</style>
<table width='600' border='0' cellpadding='0' cellspacing='1' bgcolor='#FFFFFF' class='emailtx'>
  <tr>
    <td colspan='2' class='emailtitle'><b>Customer Details</b></td>
  </tr>
  <tr>
    <td width='216' class='emailtx02'>First Name</td>
    <td width='384' class='emailtx02'>$fname</td>
  </tr>
  <tr>
    <td class='emailtx02'>Last Name</td>
    <td class='emailtx02'>$lname</td>
  </tr>
  <tr>
    <td class='emailtx02'>Phone</td>
    <td class='emailtx02'>$tel $tel02 $tel03</td>
  </tr>
  <tr>
    <td class='emailtx02'>Mobile</td>
    <td class='emailtx02'>$mobile $mobile02 $mobile03</td>
  </tr>
  <tr>
    <td class='emailtx02'>E-mail Address</td>
    <td class='emailtx02'>$email</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2' class='emailtitle'><b>Billing Address  </b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class='emailtx02'>Street No.: </td>
    <td class='emailtx02'>$bill_streetno</td>
  </tr>
  <tr>
    <td class='emailtx02'>Street Name: </td>
    <td class='emailtx02'>$bill_street</td>
  </tr>
  <tr>
    <td class='emailtx02'>City: </td>
    <td class='emailtx02'>$bill_city</td>
  </tr>
  <tr>
    <td class='emailtx02'>Apt No.: </td>
    <td class='emailtx02'>$bill_apt</td>
  </tr>
  <tr>
    <td class='emailtx02'>State: </td>
    <td class='emailtx02'>$bill_state</td>
  </tr>
  <tr>
    <td class='emailtx02'>Post code</td>
    <td class='emailtx02'>$bill_zip</td>
  </tr>
  <tr>
    <td class='emailtx02'>Country</td>
    <td class='emailtx02'>$bill_country</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2' class='emailtitle'><b>Shipping Address  </b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class='emailtx02'>Street No.: </td>
    <td class='emailtx02'>$ship_streetno</td>
  </tr>
  <tr>
    <td class='emailtx02'>Atp No. : </td>
    <td class='emailtx02'>$ship_apt</td>
  </tr>
  <tr>
    <td class='emailtx02'>Street Name: </td>
    <td class='emailtx02'>$ship_street</td>
  </tr>
  <tr>
    <td class='emailtx02'>City: </td>
    <td class='emailtx02'>$ship_city</td>
  </tr>
  <tr>
    <td class='emailtx02'>State: </td>
    <td class='emailtx02'>$ship_state</td>
  </tr>
  <tr>
    <td class='emailtx02'>Post code</td>
    <td class='emailtx02'>$ship_zip</td>
  </tr>
  <tr>
    <td class='emailtx02'>Country</td>
    <td class='emailtx02'>$ship_country</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>";
 //} 
if(trim($newfilename) != "") {  
$message =  $message."
  <tr>
    <td colspan='2' class='emailtitle'><b> Upload a Design  </b></td>
  </tr>
<tr>
    <td class='emailtitle'>Upload a design:</td>
    <td class='emailtx02'><a href='http://www.thebalmainboatcompany.com/a-photos/$newfilename'>Download</a></td>
  </tr>";
 } 
if(trim($comments) != "") {  
$message =  $message."
  <tr>
    <td colspan='2' class='emailtx02'>Comments:</td>
  </tr>
  <tr>
    <td colspan='2' class='emailtx02'>$comments</td>
  </tr>"; 
}
if(trim($CardType) != "") {   
$message =  $message."<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2' class='emailtitle'><b>Payment Information</b> </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class='emailtx02'>Creditcard Type: </td>
    <td class='emailtx02'>$CardType</td>
  </tr>
  <tr>
    <td class='emailtx02'>Credit card number: </td>
    <td class='emailtx02'>$CardNumber</td>
  </tr>
  <tr>
    <td class='emailtx02'>Expiry date: </td>
    <td class='emailtx02'>$ExpMon / $ExpYear</td>
  </tr>
  <tr>
    <td class='emailtx02'>Security code:</td>
    <td class='emailtx02'>$CardSecurity</td>
  </tr>
  <tr>
    <td class='emailtx02'>Name on credit card: </td>
    <td class='emailtx02'>$cardname</td>
  </tr>"; 
 } 
$message =  $message."</table>";	

$message  = stripslashes($message);
$body = $message;
  
$mailheaders = "Return-Path: sales@thebalmainboatcompany.com\r\n";
$mailheaders .= "From:  $email \r\n";
$mailheaders .= "Content-Type: text/html; charset=UTF-8";
mail("sales@thebalmainboatcompany.com","Purchase - ".$email,$body,$mailheaders);
mail("seounghun@gmail.com","Purchase - ".$email,$body,$mailheaders);


//////////////////////////////////////////////////////////////////////////////////////////
$body2 ="
This is the auto reply email sent to customers upon purchasing the boat.

Thank you for purchasing a Balmain Boat Company boat. 

All boats will be dispatched from our Sydney warehouse within 7-10 business days from receipt of payment. After dispatch, shipping time to most capital cities is usually 7-10 business days.

For international orders all boats will be dispatched from our overseas suppliers within 14 business days from receipt of payment. After dispatch, shipping time to most capital cities is usually 7-10 business days.

But please remember dates are estimates only and we apologize in advance for any delays that may be experienced.

Please feel free to contact us with any questions or concerns at help@thebalmainboatcompany.com.

When you�ve built your BBCo boat we want to see it, please follow us on Facebook and send us your pics. Every BBCo boat is special to us and we want to see how you�ve made the boat your own. 

Enjoy your boat and happy building.

Andrew and Nicole

The Balmain Boat Company 
Sydney, Australia


";
$mailheaders2 = "Return-Path: sales@thebalmainboatcompany.com\r\n";
$mailheaders2 .= "From:  sales@thebalmainboatcompany.com \r\n";
$mailheaders2 .= "Content-Type: text; charset=UTF-8";
mail($email,"Confirmation - The Balmain Boat Company ",$body2,$mailheaders2);
mail("seounghun@gmail.com","Confirmation - The Balmain Boat Company ",$body2,$mailheaders2);





echo "<script type='text/javascript'> window.location = 'confirmation.php'</script>";
exit;

			
	}
}

function valid_email($address) // check an email address is possibly valid 
{ 
  if (ereg('^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$', $address)) 
    return true; 
  else 
    return false; 
}

?>
<SCRIPT LANGUAGE="JavaScript">
<!-- Original:  Simon Tneoh (tneohcb@pc.jaring.my) -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
var Cards = new makeArray(8);
Cards[0] = new CardType("MasterCard", "51,52,53,54,55", "16");
var MasterCard = Cards[0];
Cards[1] = new CardType("VisaCard", "4", "13,16");
var VisaCard = Cards[1];
Cards[2] = new CardType("AmExCard", "34,37", "15");
var AmExCard = Cards[2];
Cards[3] = new CardType("DinersClubCard", "30,36,38", "14");
var DinersClubCard = Cards[3];
Cards[4] = new CardType("DiscoverCard", "6011", "16");
var DiscoverCard = Cards[4];
Cards[5] = new CardType("enRouteCard", "2014,2149", "15");
var enRouteCard = Cards[5];
Cards[6] = new CardType("JCBCard", "3088,3096,3112,3158,3337,3528", "16");
var JCBCard = Cards[6];
var LuhnCheckSum = Cards[7] = new CardType();

/*************************************************************************\
CheckCardNumber(form)
function called when users click the "check" button.
\*************************************************************************/
function CheckCardNumber(form) {
var tmpyear;
if (form.CardNumber.value.length == 0) {
alert("Please enter a Card Number.");
form.CardNumber.focus();
return;
}
if (form.ExpYear.value.length == 0) {
alert("Please enter the Expiration Year.");
form.ExpYear.focus();
return;
}
if (form.ExpYear.value > 96)
tmpyear = "19" + form.ExpYear.value;
else if (form.ExpYear.value < 21)
tmpyear = "20" + form.ExpYear.value;
else {
alert("The Expiration Year is not valid.");
return;
}
tmpmonth = form.ExpMon.options[form.ExpMon.selectedIndex].value;
// The following line doesn't work in IE3, you need to change it
// to something like "(new CardType())...".
// if (!CardType().isExpiryDate(tmpyear, tmpmonth)) {
if (!(new CardType()).isExpiryDate(tmpyear, tmpmonth)) {
alert("This card has already expired.");
return;
}
card = form.CardType.options[form.CardType.selectedIndex].value;
var retval = eval(card + ".checkCardNumber(\"" + form.CardNumber.value +
"\", " + tmpyear + ", " + tmpmonth + ");");
cardname = "";
if (retval)



// comment this out if used on an order form
alert("This card number appears to be valid.");


else {
// The cardnumber has the valid luhn checksum, but we want to know which
// cardtype it belongs to.
for (var n = 0; n < Cards.size; n++) {
if (Cards[n].checkCardNumber(form.CardNumber.value, tmpyear, tmpmonth)) {
cardname = Cards[n].getCardType();
break;
   }
}
if (cardname.length > 0) {
alert("This looks like a " + cardname + " number, not a " + card + " number.");
}
else {
alert("This card number is not valid.");
      }
   }
}
/*************************************************************************\
Object CardType([String cardtype, String rules, String len, int year, 
                                        int month])
cardtype    : type of card, eg: MasterCard, Visa, etc.
rules       : rules of the cardnumber, eg: "4", "6011", "34,37".
len         : valid length of cardnumber, eg: "16,19", "13,16".
year        : year of expiry date.
month       : month of expiry date.
eg:
var VisaCard = new CardType("Visa", "4", "16");
var AmExCard = new CardType("AmEx", "34,37", "15");
\*************************************************************************/
function CardType() {
var n;
var argv = CardType.arguments;
var argc = CardType.arguments.length;

this.objname = "object CardType";

var tmpcardtype = (argc > 0) ? argv[0] : "CardObject";
var tmprules = (argc > 1) ? argv[1] : "0,1,2,3,4,5,6,7,8,9";
var tmplen = (argc > 2) ? argv[2] : "13,14,15,16,19";

this.setCardNumber = setCardNumber;  // set CardNumber method.
this.setCardType = setCardType;  // setCardType method.
this.setLen = setLen;  // setLen method.
this.setRules = setRules;  // setRules method.
this.setExpiryDate = setExpiryDate;  // setExpiryDate method.

this.setCardType(tmpcardtype);
this.setLen(tmplen);
this.setRules(tmprules);
if (argc > 4)
this.setExpiryDate(argv[3], argv[4]);

this.checkCardNumber = checkCardNumber;  // checkCardNumber method.
this.getExpiryDate = getExpiryDate;  // getExpiryDate method.
this.getCardType = getCardType;  // getCardType method.
this.isCardNumber = isCardNumber;  // isCardNumber method.
this.isExpiryDate = isExpiryDate;  // isExpiryDate method.
this.luhnCheck = luhnCheck;// luhnCheck method.
return this;
}

/*************************************************************************\
boolean checkCardNumber([String cardnumber, int year, int month])
return true if cardnumber pass the luhncheck and the expiry date is
valid, else return false.
\*************************************************************************/
function checkCardNumber() {
var argv = checkCardNumber.arguments;
var argc = checkCardNumber.arguments.length;
var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
var year = (argc > 1) ? argv[1] : this.year;
var month = (argc > 2) ? argv[2] : this.month;

this.setCardNumber(cardnumber);
this.setExpiryDate(year, month);

if (!this.isCardNumber())
return false;
if (!this.isExpiryDate())
return false;

return true;
}
/*************************************************************************\
String getCardType()
return the cardtype.
\*************************************************************************/
function getCardType() {
return this.cardtype;
}
/*************************************************************************\
String getExpiryDate()
return the expiry date.
\*************************************************************************/
function getExpiryDate() {
return this.month + "/" + this.year;
}
/*************************************************************************\
boolean isCardNumber([String cardnumber])
return true if cardnumber pass the luhncheck and the rules, else return
false.
\*************************************************************************/
function isCardNumber() {
var argv = isCardNumber.arguments;
var argc = isCardNumber.arguments.length;
var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
if (!this.luhnCheck())
return false;

for (var n = 0; n < this.len.size; n++)
if (cardnumber.toString().length == this.len[n]) {
for (var m = 0; m < this.rules.size; m++) {
var headdigit = cardnumber.substring(0, this.rules[m].toString().length);
if (headdigit == this.rules[m])
return true;
}
return false;
}
return false;
}

/*************************************************************************\
boolean isExpiryDate([int year, int month])
return true if the date is a valid expiry date,
else return false.
\*************************************************************************/
function isExpiryDate() {
var argv = isExpiryDate.arguments;
var argc = isExpiryDate.arguments.length;

year = argc > 0 ? argv[0] : this.year;
month = argc > 1 ? argv[1] : this.month;

if (!isNum(year+""))
return false;
if (!isNum(month+""))
return false;
today = new Date();
expiry = new Date(year, month);
if (today.getTime() > expiry.getTime())
return false;
else
return true;
}

/*************************************************************************\
boolean isNum(String argvalue)
return true if argvalue contains only numeric characters,
else return false.
\*************************************************************************/
function isNum(argvalue) {
argvalue = argvalue.toString();

if (argvalue.length == 0)
return false;

for (var n = 0; n < argvalue.length; n++)
if (argvalue.substring(n, n+1) < "0" || argvalue.substring(n, n+1) > "9")
return false;

return true;
}

/*************************************************************************\
boolean luhnCheck([String CardNumber])
return true if CardNumber pass the luhn check else return false.
Reference: http://www.ling.nwu.edu/~sburke/pub/luhn_lib.pl
\*************************************************************************/
function luhnCheck() {
var argv = luhnCheck.arguments;
var argc = luhnCheck.arguments.length;

var CardNumber = argc > 0 ? argv[0] : this.cardnumber;

if (! isNum(CardNumber)) {
return false;
  }

var no_digit = CardNumber.length;
var oddoeven = no_digit & 1;
var sum = 0;

for (var count = 0; count < no_digit; count++) {
var digit = parseInt(CardNumber.charAt(count));
if (!((count & 1) ^ oddoeven)) {
digit *= 2;
if (digit > 9)
digit -= 9;
}
sum += digit;
}
if (sum % 10 == 0)
return true;
else
return false;
}

/*************************************************************************\
ArrayObject makeArray(int size)
return the array object in the size specified.
\*************************************************************************/
function makeArray(size) {
this.size = size;
return this;
}

/*************************************************************************\
CardType setCardNumber(cardnumber)
return the CardType object.
\*************************************************************************/
function setCardNumber(cardnumber) {
this.cardnumber = cardnumber;
return this;
}

/*************************************************************************\
CardType setCardType(cardtype)
return the CardType object.
\*************************************************************************/
function setCardType(cardtype) {
this.cardtype = cardtype;
return this;
}

/*************************************************************************\
CardType setExpiryDate(year, month)
return the CardType object.
\*************************************************************************/
function setExpiryDate(year, month) {
this.year = year;
this.month = month;
return this;
}

/*************************************************************************\
CardType setLen(len)
return the CardType object.

\*************************************************************************/
function setLen(len) {
// Create the len array.
if (len.length == 0 || len == null)
len = "13,14,15,16,19";

var tmplen = len;
n = 1;
while (tmplen.indexOf(",") != -1) {
tmplen = tmplen.substring(tmplen.indexOf(",") + 1, tmplen.length);
n++;
}
this.len = new makeArray(n);
n = 0;
while (len.indexOf(",") != -1) {
var tmpstr = len.substring(0, len.indexOf(","));
this.len[n] = tmpstr;
len = len.substring(len.indexOf(",") + 1, len.length);
n++;
}
this.len[n] = len;
return this;
}

/*************************************************************************\
CardType setRules()
return the CardType object.
\*************************************************************************/
function setRules(rules) {
// Create the rules array.
if (rules.length == 0 || rules == null)
rules = "0,1,2,3,4,5,6,7,8,9";
  
var tmprules = rules;
n = 1;
while (tmprules.indexOf(",") != -1) {
tmprules = tmprules.substring(tmprules.indexOf(",") + 1, tmprules.length);
n++;
}
this.rules = new makeArray(n);
n = 0;
while (rules.indexOf(",") != -1) {
var tmpstr = rules.substring(0, rules.indexOf(","));
this.rules[n] = tmpstr;
rules = rules.substring(rules.indexOf(",") + 1, rules.length);
n++;
}
this.rules[n] = rules;
return this;
}
//  End -->


// COPY 
function copy_billing(){
	if(document.ThisForm.issame.checked)
	{
		document.ThisForm.ship_streetno.value = document.ThisForm.bill_streetno.value;
		document.ThisForm.ship_street.value = document.ThisForm.bill_street.value;
		document.ThisForm.ship_apt.value = document.ThisForm.bill_apt.value;
		document.ThisForm.ship_city.value 	 = document.ThisForm.bill_city.value;
		document.ThisForm.ship_state.value =  document.ThisForm.bill_state.value;
		document.ThisForm.ship_zip.value	= document.ThisForm.bill_zip.value;
		document.ThisForm.ship_country.value	= document.ThisForm.bill_country.value;
	} else
	{
		document.ThisForm.ship_streetno.value 		= "";
		document.ThisForm.ship_street.value 		= "";
		document.ThisForm.ship_apt.value 		= "";
		document.ThisForm.ship_city.value 		= "";
		document.ThisForm.ship_state.value 		= "";
		document.ThisForm.ship_zip.value 		= "";
		document.ThisForm.ship_country.value 		= "";
	}
}

</script>

<!-- lightbox -->
	<script type="text/javascript" src="a-js/jquery.min.js" ></script>
	<script type="text/javascript" src="a-js/jquery-ui.min.js" ></script>
	<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen" />
	<script type="text/javascript">
		$(document).ready(function() {

	$("#lightbox01").fancybox({
        'autoScale'     	: false,
        'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});
		
	$("#lightbox02").fancybox({
        'autoScale'     	: true,
        'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});
	$("#lightbox03").fancybox({
        'autoScale'     	: true,
        'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});
		
		});
	</script>
<!-- end light box -->
<style type="text/css">
<!--
.style1 {
	font-size: 16px;
	font-weight: bold;
}
-->
</style>
<form action="<? echo $_SERVER['PHP_SELF']; ?>" method="post" name="ThisForm"  enctype="multipart/form-data">
<input name="isSubmit" type="hidden" value="1" />

<table width="850" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2" class="nTitle01">Thanks for your interest in The Balmain  Boat Company.&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><br />
      We are currently only selling the BBCo Original Boat Kit to find out about this boat <a href="products.htm" target="_blank">click here</a>.<br />
      <br />
      The boat is priced at $1599 discounted from $2500 excluding shipping costs and taxes. To find out shipping costs <a id="lightbox02" href="http://www.thebalmainboatcompany.com/shipping.php" title="">click here</a>.&nbsp;<br />
      <br />
      To purchase the boat please fill in the order form below or <a href="mailto:info@thebalmainboatcompany.com">contact us</a> directly.<br />
<br />
<?php if($ERRORS != "") {?>
<br />
<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF0000">
  <tr>
    <td bgcolor="#FFAEAE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="6%" height="40" align="center" valign="middle"><img src="images/alert.png" width="32" height="32" /></td>
        <td width="94%"><strong>Looks like you've missed part of the form, please fill in the missing fields highlighted in red.</strong></td>
      </tr>
    </table>      </td>
  </tr>
</table>
<br />
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="right" ><em><span class="style1">* </span>required fields</em> </td>
  </tr>
</table>

</td>
  </tr>
  <tr>
    <td width="50%" height="28" class="nStrip">
	<table width="300" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="28" class="ntitle02">Customer Details</td>
        <td width="20"><img src="images/a-Rconner.jpg" width="20" height="28" /></td>
      </tr>
    </table></td>
    <td class="nStrip"><table width="300" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="28" class="ntitle02">Upload a Design
</td>
        <td width="20"><img src="images/a-Rconner.jpg" width="20" height="28" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10" class="nBrdLRB" id="nPadding"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" height="25"><?php if($ERRORS['fname']) echo $error_open; ?>First Name *<?php if($ERRORS['fname']) echo $error_end; ?></td>
        <td width="70%"><input name="fname" type="text" id="boxSize" value="<? echo $_POST["fname"]; ?>" /></td>
      </tr>
      <tr>
        <td height="25"><?php if($ERRORS['lname']) echo $error_open; ?>Last Name *<?php if($ERRORS['lname']) echo $error_end; ?></td>
        <td><input name="lname" type="text" id="boxSize" value="<? echo $_POST["lname"]; ?>" /></td>
      </tr>
      <tr>
        <td height="25">&nbsp;</td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="27%" id="telinfo">country<br />
              code</td>
            <td width="41%" id="telinfo">area code </td>
            <td width="32%" id="telinfo">number</td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td height="25"><?php if($ERRORS['tel']) echo $error_open; ?>Phone *<?php if($ERRORS['tel']) echo $error_end; ?></td>
        <td>
		<input name="tel" type="text" class="box-tel01" value="<? echo $_POST["tel"]; ?>"   />
        <input name="tel02" type="text" class="box-tel02" value="<? echo $_POST["tel02"]; ?>"   />
        <input name="tel03" type="text" class="box-tel03" value="<? echo $_POST["tel03"]; ?>"  />		  </td>
      </tr>
      <tr>
        <td height="25">Mobile</td>
        <td>
		<input name="mobile" type="text" class="box-tel01" value="<? echo $_POST["mobile"]; ?>"  />
		<input name="mobile02" type="text" class="box-tel02" value="<? echo $_POST["mobile02"]; ?>"  />
		<input name="mobile03" type="text" class="box-tel03" value="<? echo $_POST["mobile03"]; ?>"  />		</td>
      </tr>
      <tr>
        <td height="25"><?php if($ERRORS['email']) echo $error_open; ?>E-mail Address *<?php if($ERRORS['email']) echo $error_end; ?></td>
        <td><input name="email" type="text" id="boxSize" value="<? echo $_POST["email"]; ?>" /></td>
      </tr>
      <tr>
        <td height="25"><?php if($ERRORS['email02']) echo $error_open; ?>Confirm E-mail *<?php if($ERRORS['email02']) echo $error_end; ?></td>
        <td><input name="email02" type="text" id="boxSize" value="<? echo $_POST["email02"]; ?>" /></td>
      </tr>
    </table></td>
    <td valign="top" class="nBrdRB" id="nPadding">
	  <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#FCB134">
  <tr>
    <td height="40" bgcolor="#FFFFFF">&nbsp;&nbsp;<b>Upload your own design of a figurehead. 
      </b><br />
      &nbsp;&nbsp;<b>For more info <a href="products.htm" target="_blank">click here</a>.	 </b></td>
  </tr>
</table>

	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" height="25"><?php if($ERRORS['photo']) echo $error_open; ?>Upload a design
  <?php if($ERRORS['photo']) echo $error_end; ?></td>
        <td width="70%"><input type="file" name="photo"/></td>
      </tr>

      <tr>
        <td height="14" align="right" id="telinfo"></td>
        <td height="14" align="left" id="telinfo">(Please upload only files in JPG or GIFformat.)</td>
      </tr>
      <tr>
        <td height="25">Comments</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2"><textarea name="comments" cols="50" rows="5" id="comments"><? echo $_POST["comments"]; ?></textarea></td>
        </tr>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="50%" height="28" class="nStrip">
	<table width="300" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="28" class="ntitle02">Billing   Address&nbsp; </td>
        <td width="20"><img src="images/a-Rconner.jpg" width="20" height="28" /></td>
      </tr>
    </table></td>
    <td class="nStrip"><table width="300" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="28" class="ntitle02">Shipping    Address&nbsp; </td>
        <td width="20"><img src="images/a-Rconner.jpg" width="20" height="28" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="120" class="nBrdLRB" id="nPadding"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" height="25">&nbsp;</td>
        <td width="70%">&nbsp;</td>
      </tr>
      <tr>
        <td width="30%" height="25"><?php if($ERRORS['bill_streetno']) echo $error_open; ?>Street No.*<?php if($ERRORS['bill_streetno']) echo $error_end; ?></td>
        <td width="70%"><input name="bill_streetno" type="text" id="boxSize" value="<? echo $_POST["bill_streetno"]; ?>" /></td>
      </tr>
      <tr>
        <td width="30%" height="25">Apt. Number </td>
        <td width="70%"><input name="bill_apt" type="text" id="boxSize" value="<? echo $_POST["bill_apt"]; ?>" /></td>
      </tr>
      <tr>
        <td width="30%" height="25"><?php if($ERRORS['bill_street']) echo $error_open; ?>Street Name*<?php if($ERRORS['bill_street']) echo $error_end; ?></td>
        <td width="70%"><input name="bill_street" type="text" id="boxSize" value="<? echo $_POST["bill_street"]; ?>" /></td>
      </tr>
      <tr>
        <td height="25"><?php if($ERRORS['bill_city']) echo $error_open; ?>City *<?php if($ERRORS['bill_city']) echo $error_end; ?></td>
        <td> <input name="bill_city" type="text" id="boxSize" value="<? echo $_POST["bill_city"]; ?>" /></td>
      </tr>
      <tr>
        <td height="25"><?php if($ERRORS['bill_state']) echo $error_open; ?>State *<?php if($ERRORS['bill_state']) echo $error_end; ?></td>
        <td><input name="bill_state" type="text" id="boxSize" value="<? echo $_POST["bill_state"]; ?>" /></td>
      </tr>
      <tr>
        <td height="25"><?php if($ERRORS['bill_zip']) echo $error_open; ?>post / zip code
 *<?php if($ERRORS['bill_zip']) echo $error_end; ?></td>
        <td><input name="bill_zip" type="text" id="boxSize" value="<? echo $_POST["bill_zip"]; ?>" /></td>
      </tr>

      <tr>
        <td><?php if($ERRORS['bill_country']) echo $error_open; ?>Country *
            <?php if($ERRORS['bill_country']) echo $error_end; ?></td>
        <td><select name="bill_country"  id="boxSize">
      <option value="">Select...</option>
 
      <option value="Australia" >Australia</option>
	
      <option value="USA" >USA</option>
      <option value="" >-----------------------------------------------------</option>
	
      <option value="Afghanistan" >Afghanistan</option>
	
      <option value="Albania" >Albania</option>
	
      <option value="Algeria" >Algeria</option>
	
      <option value="American Samoa" >American Samoa</option>
	
      <option value="Andorra" >Andorra</option>
	
      <option value="Angola" >Angola</option>
	
      <option value="Anguilla" >Anguilla</option>
	
      <option value="Antarctica" >Antarctica</option>
	
      <option value="Antigua and Barbuda" >Antigua and Barbuda</option>
	
      <option value="Arctic Ocean" >Arctic Ocean</option>
	
      <option value="Argentina" >Argentina</option>
	
      <option value="Armenia" >Armenia</option>
	
      <option value="Aruba" >Aruba</option>
	
      <option value="Ashmore and Cartier Islands" >Ashmore and Cartier Islands</option>
	
      <option value="Atlantic Ocean" >Atlantic Ocean</option>
	
      <option value="Austria" >Austria</option>
	
      <option value="Azerbaijan" >Azerbaijan</option>
	
      <option value="Bahamas" >Bahamas</option>
	
      <option value="Bahrain" >Bahrain</option>
	
      <option value="Baker Island" >Baker Island</option>
	
      <option value="Bangladesh" >Bangladesh</option>
	
      <option value="Barbados" >Barbados</option>
	
      <option value="Bassas da India" >Bassas da India</option>
	
      <option value="Belarus" >Belarus</option>
	
      <option value="Belgium" >Belgium</option>
	
      <option value="Belize" >Belize</option>
	
      <option value="Benin" >Benin</option>
	
      <option value="Bermuda" >Bermuda</option>
	
      <option value="Bhutan" >Bhutan</option>
	
      <option value="Bolivia" >Bolivia</option>
	
      <option value="Borneo" >Borneo</option>
	
      <option value="Bosnia and Herzegovina" >Bosnia and Herzegovina</option>
	
      <option value="Botswana" >Botswana</option>
	
      <option value="Bouvet Island" >Bouvet Island</option>
	
      <option value="Brazil" >Brazil</option>
	
      <option value="British Virgin Islands" >British Virgin Islands</option>
	
      <option value="Brunei" >Brunei</option>
	
      <option value="Bulgaria" >Bulgaria</option>
	
      <option value="Burkina Faso" >Burkina Faso</option>
	
      <option value="Burundi" >Burundi</option>
	
      <option value="Cambodia" >Cambodia</option>
	
      <option value="Cameroon" >Cameroon</option>
	
      <option value="Canada" >Canada</option>
	
      <option value="Cape Verde" >Cape Verde</option>
	
      <option value="Cayman Islands" >Cayman Islands</option>
	
      <option value="Central African Republic" >Central African Republic</option>
	
      <option value="Chad" >Chad</option>
	
      <option value="Chile" >Chile</option>
	
      <option value="China" >China</option>
	
      <option value="Christmas Island" >Christmas Island</option>
	
      <option value="Clipperton Island" >Clipperton Island</option>
	
      <option value="Cocos Islands" >Cocos Islands</option>
	
      <option value="Colombia" >Colombia</option>
	
      <option value="Comoros" >Comoros</option>
	
      <option value="Cook Islands" >Cook Islands</option>
	
      <option value="Coral Sea Islands" >Coral Sea Islands</option>
	
      <option value="Costa Rica" >Costa Rica</option>
	
      <option value="Cote d'Ivoire" >Cote d'Ivoire</option>
	
      <option value="Croatia" >Croatia</option>
	
      <option value="Cuba" >Cuba</option>
	
      <option value="Cyprus" >Cyprus</option>
	
      <option value="Czech Republic" >Czech Republic</option>
	
      <option value="Democratic Republic of the Congo" >Democratic Republic of the Congo</option>
	
      <option value="Denmark" >Denmark</option>
	
      <option value="Djibouti" >Djibouti</option>
	
      <option value="Dominica" >Dominica</option>
	
      <option value="Dominican Republic" >Dominican Republic</option>
	
      <option value="East Timor" >East Timor</option>
	
      <option value="Ecuador" >Ecuador</option>
	
      <option value="Egypt" >Egypt</option>
	
      <option value="El Salvador" >El Salvador</option>
	
      <option value="Equatorial Guinea" >Equatorial Guinea</option>
	
      <option value="Eritrea" >Eritrea</option>
	
      <option value="Estonia" >Estonia</option>
	
      <option value="Ethiopia" >Ethiopia</option>
	
      <option value="Europa Island" >Europa Island</option>
	
      <option value="Falkland Islands (Islas Malvinas)" >Falkland Islands (Islas Malvinas)</option>
	
      <option value="Faroe Islands" >Faroe Islands</option>
	
      <option value="Fiji" >Fiji</option>
	
      <option value="Finland" >Finland</option>
	
      <option value="France" >France</option>
	
      <option value="French Guiana" >French Guiana</option>
	
      <option value="French Polynesia" >French Polynesia</option>
	
      <option value="French Southern and Antarctic Lands" >French Southern and Antarctic Lands</option>
	
      <option value="Gabon" >Gabon</option>
	
      <option value="Gambia" >Gambia</option>
	
      <option value="Gaza Strip" >Gaza Strip</option>
	
      <option value="Georgia" >Georgia</option>
	
      <option value="Germany" >Germany</option>
	
      <option value="Ghana" >Ghana</option>
	
      <option value="Gibraltar" >Gibraltar</option>
	
      <option value="Glorioso Islands" >Glorioso Islands</option>
	
      <option value="Greece" >Greece</option>
	
      <option value="Greenland" >Greenland</option>
	
      <option value="Grenada" >Grenada</option>
	
      <option value="Guadeloupe" >Guadeloupe</option>
	
      <option value="Guam" >Guam</option>
	
      <option value="Guatemala" >Guatemala</option>
	
      <option value="Guernsey" >Guernsey</option>
	
      <option value="Guinea" >Guinea</option>
	
      <option value="Guinea-Bissau" >Guinea-Bissau</option>
	
      <option value="Guyana" >Guyana</option>
	
      <option value="Haiti" >Haiti</option>
	
      <option value="Heard Island and McDonald Islands" >Heard Island and McDonald Islands</option>
	
      <option value="Honduras" >Honduras</option>
	
      <option value="Hong Kong" >Hong Kong</option>
	
      <option value="Howland Island" >Howland Island</option>
	
      <option value="Hungary" >Hungary</option>
	
      <option value="Iceland" >Iceland</option>
	
      <option value="India" >India</option>
	
      <option value="Indian Ocean" >Indian Ocean</option>
	
      <option value="Indonesia" >Indonesia</option>
	
      <option value="Iran" >Iran</option>
	
      <option value="Iraq" >Iraq</option>
	
      <option value="Ireland" >Ireland</option>
	
      <option value="Isle of Man" >Isle of Man</option>
	
      <option value="Israel" >Israel</option>
	
      <option value="Italy" >Italy</option>
	
      <option value="Jamaica" >Jamaica</option>
	
      <option value="Jan Mayen" >Jan Mayen</option>
	
      <option value="Japan" >Japan</option>
	
      <option value="Jarvis Island" >Jarvis Island</option>
	
      <option value="Jersey" >Jersey</option>
	
      <option value="Johnston Atoll" >Johnston Atoll</option>
	
      <option value="Jordan" >Jordan</option>
	
      <option value="Juan de Nova Island" >Juan de Nova Island</option>
	
      <option value="Kazakhstan" >Kazakhstan</option>
	
      <option value="Kenya" >Kenya</option>
	
      <option value="Kerguelen Archipelago" >Kerguelen Archipelago</option>
	
      <option value="Kingman Reef" >Kingman Reef</option>
	
      <option value="Kiribati" >Kiribati</option>
	
      <option value="Kosovo" >Kosovo</option>
	
      <option value="Kuwait" >Kuwait</option>
	
      <option value="Kyrgyzstan" >Kyrgyzstan</option>
	
      <option value="Laos" >Laos</option>
	
      <option value="Latvia" >Latvia</option>
	
      <option value="Lebanon" >Lebanon</option>
	
      <option value="Lesotho" >Lesotho</option>
	
      <option value="Liberia" >Liberia</option>
	
      <option value="Libya" >Libya</option>
	
      <option value="Liechtenstein" >Liechtenstein</option>
	
      <option value="Lithuania" >Lithuania</option>
	
      <option value="Luxembourg" >Luxembourg</option>
	
      <option value="Macau" >Macau</option>
	
      <option value="Macedonia" >Macedonia</option>
	
      <option value="Madagascar" >Madagascar</option>
	
      <option value="Malawi" >Malawi</option>
	
      <option value="Malaysia" >Malaysia</option>
	
      <option value="Maldives" >Maldives</option>
	
      <option value="Mali" >Mali</option>
	
      <option value="Malta" >Malta</option>
	
      <option value="Marshall Islands" >Marshall Islands</option>
	
      <option value="Martinique" >Martinique</option>
	
      <option value="Mauritania" >Mauritania</option>
	
      <option value="Mauritius" >Mauritius</option>
	
      <option value="Mayotte" >Mayotte</option>
	
      <option value="Mediterranean Sea" >Mediterranean Sea</option>
	
      <option value="Mexico" >Mexico</option>
	
      <option value="Micronesia" >Micronesia</option>
	
      <option value="Midway Islands" >Midway Islands</option>
	
      <option value="Moldova" >Moldova</option>
	
      <option value="Monaco" >Monaco</option>
	
      <option value="Mongolia" >Mongolia</option>
	
      <option value="Montenegro" >Montenegro</option>
	
      <option value="Montserrat" >Montserrat</option>
	
      <option value="Morocco" >Morocco</option>
	
      <option value="Mozambique" >Mozambique</option>
	
      <option value="Myanmar" >Myanmar</option>
	
      <option value="Namibia" >Namibia</option>
	
      <option value="Nauru" >Nauru</option>
	
      <option value="Navassa Island" >Navassa Island</option>
	
      <option value="Nepal" >Nepal</option>
	
      <option value="Netherlands" >Netherlands</option>
	
      <option value="Netherlands Antilles" >Netherlands Antilles</option>
	
      <option value="New Caledonia" >New Caledonia</option>
	
      <option value="New Zealand" >New Zealand</option>
	
      <option value="Nicaragua" >Nicaragua</option>
	
      <option value="Niger" >Niger</option>
	
      <option value="Nigeria" >Nigeria</option>
	
      <option value="Niue" >Niue</option>
	
      <option value="Norfolk Island" >Norfolk Island</option>
	
      <option value="North Korea" >North Korea</option>
	
      <option value="North Sea" >North Sea</option>
	
      <option value="Northern Mariana Islands" >Northern Mariana Islands</option>
	
      <option value="Norway" >Norway</option>
	
      <option value="Oman" >Oman</option>
	
      <option value="Pacific Ocean" >Pacific Ocean</option>
	
      <option value="Pakistan" >Pakistan</option>
	
      <option value="Palau" >Palau</option>
	
      <option value="Palestine " >Palestine </option>
	
      <option value="Palmyra Atoll" >Palmyra Atoll</option>
	
      <option value="Panama" >Panama</option>
	
      <option value="Papua New Guinea" >Papua New Guinea</option>
	
      <option value="Paracel Islands" >Paracel Islands</option>
	
      <option value="Paraguay" >Paraguay</option>
	
      <option value="Peru" >Peru</option>
	
      <option value="Philippines" >Philippines</option>
	
      <option value="Pitcairn Islands" >Pitcairn Islands</option>
	
      <option value="Poland" >Poland</option>
	
      <option value="Portugal" >Portugal</option>
	
      <option value="Puerto Rico" >Puerto Rico</option>
	
      <option value="Qatar" >Qatar</option>
	
      <option value="Republic of the Congo" >Republic of the Congo</option>
	
      <option value="Reunion" >Reunion</option>
	
      <option value="Romania" >Romania</option>
	
      <option value="Ross Sea" >Ross Sea</option>
	
      <option value="Russia" >Russia</option>
	
      <option value="Rwanda" >Rwanda</option>
	
      <option value="Saint Helena" >Saint Helena</option>
	
      <option value="Saint Kitts and Nevis" >Saint Kitts and Nevis</option>
	
      <option value="Saint Lucia" >Saint Lucia</option>
	
      <option value="Saint Pierre and Miquelon" >Saint Pierre and Miquelon</option>
	
      <option value="Saint Vincent and the Grenadines" >Saint Vincent and the Grenadines</option>
	
      <option value="Samoa" >Samoa</option>
	
      <option value="San Marino" >San Marino</option>
	
      <option value="Sao Tome and Principe" >Sao Tome and Principe</option>
	
      <option value="Saudi Arabia" >Saudi Arabia</option>
	
      <option value="Senegal" >Senegal</option>
	
      <option value="Serbia" >Serbia</option>
	
      <option value="Seychelles" >Seychelles</option>
	
      <option value="Sierra Leone" >Sierra Leone</option>
	
      <option value="Singapore" >Singapore</option>
	
      <option value="Slovakia" >Slovakia</option>
	
      <option value="Slovenia" >Slovenia</option>
	
      <option value="Solomon Islands" >Solomon Islands</option>
	
      <option value="Somalia" >Somalia</option>
	
      <option value="South Africa" >South Africa</option>
	
      <option value="South Georgia and the South Sandwich Islands" >South Georgia and the South Sandwich Islands</option>
	
      <option value="South Korea" >South Korea</option>
	
      <option value="Southern Ocean" >Southern Ocean</option>
	
      <option value="Spain" >Spain</option>
	
      <option value="Spratly Islands" >Spratly Islands</option>
	
      <option value="Sri Lanka" >Sri Lanka</option>
	
      <option value="Sudan" >Sudan</option>
	
      <option value="Suriname" >Suriname</option>
	
      <option value="Svalbard" >Svalbard</option>
	
      <option value="Swaziland" >Swaziland</option>
	
      <option value="Sweden" >Sweden</option>
	
      <option value="Switzerland" >Switzerland</option>
	
      <option value="Syria" >Syria</option>
	
      <option value="Taiwan" >Taiwan</option>
	
      <option value="Tajikistan" >Tajikistan</option>
	
      <option value="Tanzania" >Tanzania</option>
	
      <option value="Tasman Sea" >Tasman Sea</option>
	
      <option value="Thailand" >Thailand</option>
	
      <option value="Togo" >Togo</option>
	
      <option value="Tokelau" >Tokelau</option>
	
      <option value="Tonga" >Tonga</option>
	
      <option value="Trinidad and Tobago" >Trinidad and Tobago</option>
	
      <option value="Tromelin Island" >Tromelin Island</option>
	
      <option value="Tunisia" >Tunisia</option>
	
      <option value="Turkey" >Turkey</option>
	
      <option value="Turkmenistan" >Turkmenistan</option>
	
      <option value="Turks and Caicos Islands" >Turks and Caicos Islands</option>
	
      <option value="Tuvalu" >Tuvalu</option>
	
      <option value="Uganda" >Uganda</option>
	
      <option value="Ukraine" >Ukraine</option>
	
      <option value="United Arab Emirates" >United Arab Emirates</option>
	
      <option value="United Kingdom" >United Kingdom</option>
	
      <option value="Uruguay" >Uruguay</option>
	
      <option value="Uzbekistan" >Uzbekistan</option>
	
      <option value="Vanuatu" >Vanuatu</option>
	
      <option value="Venezuela" >Venezuela</option>
	
      <option value="Viet Nam" >Viet Nam</option>
	
      <option value="Virgin Islands" >Virgin Islands</option>
	
      <option value="Wake Island" >Wake Island</option>
	
      <option value="Wallis and Futuna" >Wallis and Futuna</option>
	
      <option value="West Bank" >West Bank</option>
	
      <option value="Western Sahara" >Western Sahara</option>
	
      <option value="Yemen" >Yemen</option>
	
      <option value="Zambia" >Zambia</option>
	
      <option value="Zimbabwe" >Zimbabwe</option>
	  
    </select></td>
      </tr>
    </table></td>
    <td class="nBrdRB" id="nPadding"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="25" colspan="2" bgcolor="#CCCCCC"><input name="issame" id="issame" type="checkbox" value="y" onClick="copy_billing();" tabindex="18" <?php if($issame) echo "checked"; ?>> <font color="#666666">
          <label for="issame">Same as my billing information</label> </td>
        </tr>
      <tr>
        <td width="30%" height="25"><?php if($ERRORS['ship_streetno']) echo $error_open; ?>Street No.*<?php if($ERRORS['ship_streetno']) echo $error_end; ?></td>
        <td width="70%"><input name="ship_streetno" type="text" id="boxSize" value="<? echo $_POST["ship_streetno"]; ?>" /></td>
      </tr>
      <tr>
        <td width="30%" height="25">Apt. Number</td>
        <td width="70%"><input name="ship_apt" type="text" id="boxSize" value="<? echo $_POST["ship_apt"]; ?>" /></td>
      </tr>
      <tr>
        <td width="30%" height="25"><?php if($ERRORS['ship_street']) echo $error_open; ?>Street Name*<?php if($ERRORS['ship_street']) echo $error_end; ?></td>
        <td width="70%"><input name="ship_street" type="text" id="boxSize" value="<? echo $_POST["ship_street"]; ?>" /></td>
      </tr>
      <tr>
        <td height="25"><?php if($ERRORS['ship_city']) echo $error_open; ?>City *<?php if($ERRORS['ship_city']) echo $error_end; ?></td>
        <td><input name="ship_city" type="text" id="boxSize" value="<? echo $_POST["ship_city"]; ?>" /></td>
      </tr>
      <tr>
        <td height="25"><?php if($ERRORS['ship_state']) echo $error_open; ?>State *<?php if($ERRORS['ship_state']) echo $error_end; ?></td>
        <td><input name="ship_state" type="text" id="boxSize" value="<? echo $_POST["ship_state"]; ?>" /></td>
      </tr>
      <tr>
        <td height="25"><?php if($ERRORS['ship_zip']) echo $error_open; ?>post / zip code
 *<?php if($ERRORS['ship_zip']) echo $error_end; ?></td>
        <td><input name="ship_zip" type="text" id="boxSize" value="<? echo $_POST["ship_zip"]; ?>" /></td>
      </tr>
      <tr>
        <td><?php if($ERRORS['ship_country']) echo $error_open; ?>Country *
            <?php if($ERRORS['ship_country']) echo $error_end; ?></td>
        <td><select name="ship_country"  id="boxSize">
      <option value="">Select...</option>
	
 	
      <option value="Australia" >Australia</option>
       <option value="USA" >USA</option>
       <option value="" >-----------------------------------------------------</option>
   <option value="Afghanistan" >Afghanistan</option>
	
      <option value="Albania" >Albania</option>
	
      <option value="Algeria" >Algeria</option>
	
      <option value="American Samoa" >American Samoa</option>
	
      <option value="Andorra" >Andorra</option>
	
      <option value="Angola" >Angola</option>
	
      <option value="Anguilla" >Anguilla</option>
	
      <option value="Antarctica" >Antarctica</option>
	
      <option value="Antigua and Barbuda" >Antigua and Barbuda</option>
	
      <option value="Arctic Ocean" >Arctic Ocean</option>
	
      <option value="Argentina" >Argentina</option>
	
      <option value="Armenia" >Armenia</option>
	
      <option value="Aruba" >Aruba</option>
	
      <option value="Ashmore and Cartier Islands" >Ashmore and Cartier Islands</option>
	
      <option value="Atlantic Ocean" >Atlantic Ocean</option>
	
      <option value="Austria" >Austria</option>
	
      <option value="Azerbaijan" >Azerbaijan</option>
	
      <option value="Bahamas" >Bahamas</option>
	
      <option value="Bahrain" >Bahrain</option>
	
      <option value="Baker Island" >Baker Island</option>
	
      <option value="Bangladesh" >Bangladesh</option>
	
      <option value="Barbados" >Barbados</option>
	
      <option value="Bassas da India" >Bassas da India</option>
	
      <option value="Belarus" >Belarus</option>
	
      <option value="Belgium" >Belgium</option>
	
      <option value="Belize" >Belize</option>
	
      <option value="Benin" >Benin</option>
	
      <option value="Bermuda" >Bermuda</option>
	
      <option value="Bhutan" >Bhutan</option>
	
      <option value="Bolivia" >Bolivia</option>
	
      <option value="Borneo" >Borneo</option>
	
      <option value="Bosnia and Herzegovina" >Bosnia and Herzegovina</option>
	
      <option value="Botswana" >Botswana</option>
	
      <option value="Bouvet Island" >Bouvet Island</option>
	
      <option value="Brazil" >Brazil</option>
	
      <option value="British Virgin Islands" >British Virgin Islands</option>
	
      <option value="Brunei" >Brunei</option>
	
      <option value="Bulgaria" >Bulgaria</option>
	
      <option value="Burkina Faso" >Burkina Faso</option>
	
      <option value="Burundi" >Burundi</option>
	
      <option value="Cambodia" >Cambodia</option>
	
      <option value="Cameroon" >Cameroon</option>
	
      <option value="Canada" >Canada</option>
	
      <option value="Cape Verde" >Cape Verde</option>
	
      <option value="Cayman Islands" >Cayman Islands</option>
	
      <option value="Central African Republic" >Central African Republic</option>
	
      <option value="Chad" >Chad</option>
	
      <option value="Chile" >Chile</option>
	
      <option value="China" >China</option>
	
      <option value="Christmas Island" >Christmas Island</option>
	
      <option value="Clipperton Island" >Clipperton Island</option>
	
      <option value="Cocos Islands" >Cocos Islands</option>
	
      <option value="Colombia" >Colombia</option>
	
      <option value="Comoros" >Comoros</option>
	
      <option value="Cook Islands" >Cook Islands</option>
	
      <option value="Coral Sea Islands" >Coral Sea Islands</option>
	
      <option value="Costa Rica" >Costa Rica</option>
	
      <option value="Cote d'Ivoire" >Cote d'Ivoire</option>
	
      <option value="Croatia" >Croatia</option>
	
      <option value="Cuba" >Cuba</option>
	
      <option value="Cyprus" >Cyprus</option>
	
      <option value="Czech Republic" >Czech Republic</option>
	
      <option value="Democratic Republic of the Congo" >Democratic Republic of the Congo</option>
	
      <option value="Denmark" >Denmark</option>
	
      <option value="Djibouti" >Djibouti</option>
	
      <option value="Dominica" >Dominica</option>
	
      <option value="Dominican Republic" >Dominican Republic</option>
	
      <option value="East Timor" >East Timor</option>
	
      <option value="Ecuador" >Ecuador</option>
	
      <option value="Egypt" >Egypt</option>
	
      <option value="El Salvador" >El Salvador</option>
	
      <option value="Equatorial Guinea" >Equatorial Guinea</option>
	
      <option value="Eritrea" >Eritrea</option>
	
      <option value="Estonia" >Estonia</option>
	
      <option value="Ethiopia" >Ethiopia</option>
	
      <option value="Europa Island" >Europa Island</option>
	
      <option value="Falkland Islands (Islas Malvinas)" >Falkland Islands (Islas Malvinas)</option>
	
      <option value="Faroe Islands" >Faroe Islands</option>
	
      <option value="Fiji" >Fiji</option>
	
      <option value="Finland" >Finland</option>
	
      <option value="France" >France</option>
	
      <option value="French Guiana" >French Guiana</option>
	
      <option value="French Polynesia" >French Polynesia</option>
	
      <option value="French Southern and Antarctic Lands" >French Southern and Antarctic Lands</option>
	
      <option value="Gabon" >Gabon</option>
	
      <option value="Gambia" >Gambia</option>
	
      <option value="Gaza Strip" >Gaza Strip</option>
	
      <option value="Georgia" >Georgia</option>
	
      <option value="Germany" >Germany</option>
	
      <option value="Ghana" >Ghana</option>
	
      <option value="Gibraltar" >Gibraltar</option>
	
      <option value="Glorioso Islands" >Glorioso Islands</option>
	
      <option value="Greece" >Greece</option>
	
      <option value="Greenland" >Greenland</option>
	
      <option value="Grenada" >Grenada</option>
	
      <option value="Guadeloupe" >Guadeloupe</option>
	
      <option value="Guam" >Guam</option>
	
      <option value="Guatemala" >Guatemala</option>
	
      <option value="Guernsey" >Guernsey</option>
	
      <option value="Guinea" >Guinea</option>
	
      <option value="Guinea-Bissau" >Guinea-Bissau</option>
	
      <option value="Guyana" >Guyana</option>
	
      <option value="Haiti" >Haiti</option>
	
      <option value="Heard Island and McDonald Islands" >Heard Island and McDonald Islands</option>
	
      <option value="Honduras" >Honduras</option>
	
      <option value="Hong Kong" >Hong Kong</option>
	
      <option value="Howland Island" >Howland Island</option>
	
      <option value="Hungary" >Hungary</option>
	
      <option value="Iceland" >Iceland</option>
	
      <option value="India" >India</option>
	
      <option value="Indian Ocean" >Indian Ocean</option>
	
      <option value="Indonesia" >Indonesia</option>
	
      <option value="Iran" >Iran</option>
	
      <option value="Iraq" >Iraq</option>
	
      <option value="Ireland" >Ireland</option>
	
      <option value="Isle of Man" >Isle of Man</option>
	
      <option value="Israel" >Israel</option>
	
      <option value="Italy" >Italy</option>
	
      <option value="Jamaica" >Jamaica</option>
	
      <option value="Jan Mayen" >Jan Mayen</option>
	
      <option value="Japan" >Japan</option>
	
      <option value="Jarvis Island" >Jarvis Island</option>
	
      <option value="Jersey" >Jersey</option>
	
      <option value="Johnston Atoll" >Johnston Atoll</option>
	
      <option value="Jordan" >Jordan</option>
	
      <option value="Juan de Nova Island" >Juan de Nova Island</option>
	
      <option value="Kazakhstan" >Kazakhstan</option>
	
      <option value="Kenya" >Kenya</option>
	
      <option value="Kerguelen Archipelago" >Kerguelen Archipelago</option>
	
      <option value="Kingman Reef" >Kingman Reef</option>
	
      <option value="Kiribati" >Kiribati</option>
	
      <option value="Kosovo" >Kosovo</option>
	
      <option value="Kuwait" >Kuwait</option>
	
      <option value="Kyrgyzstan" >Kyrgyzstan</option>
	
      <option value="Laos" >Laos</option>
	
      <option value="Latvia" >Latvia</option>
	
      <option value="Lebanon" >Lebanon</option>
	
      <option value="Lesotho" >Lesotho</option>
	
      <option value="Liberia" >Liberia</option>

	
      <option value="Libya" >Libya</option>
	
      <option value="Liechtenstein" >Liechtenstein</option>
	
      <option value="Lithuania" >Lithuania</option>
	
      <option value="Luxembourg" >Luxembourg</option>
	
      <option value="Macau" >Macau</option>
	
      <option value="Macedonia" >Macedonia</option>
	
      <option value="Madagascar" >Madagascar</option>
	
      <option value="Malawi" >Malawi</option>
	
      <option value="Malaysia" >Malaysia</option>
	
      <option value="Maldives" >Maldives</option>
	
      <option value="Mali" >Mali</option>
	
      <option value="Malta" >Malta</option>
	
      <option value="Marshall Islands" >Marshall Islands</option>
	
      <option value="Martinique" >Martinique</option>
	
      <option value="Mauritania" >Mauritania</option>
	
      <option value="Mauritius" >Mauritius</option>
	
      <option value="Mayotte" >Mayotte</option>
	
      <option value="Mediterranean Sea" >Mediterranean Sea</option>
	
      <option value="Mexico" >Mexico</option>
	
      <option value="Micronesia" >Micronesia</option>
	
      <option value="Midway Islands" >Midway Islands</option>
	
      <option value="Moldova" >Moldova</option>
	
      <option value="Monaco" >Monaco</option>
	
      <option value="Mongolia" >Mongolia</option>
	
      <option value="Montenegro" >Montenegro</option>
	
      <option value="Montserrat" >Montserrat</option>
	
      <option value="Morocco" >Morocco</option>
	
      <option value="Mozambique" >Mozambique</option>
	
      <option value="Myanmar" >Myanmar</option>
	
      <option value="Namibia" >Namibia</option>
	
      <option value="Nauru" >Nauru</option>
	
      <option value="Navassa Island" >Navassa Island</option>
	
      <option value="Nepal" >Nepal</option>
	
      <option value="Netherlands" >Netherlands</option>
	
      <option value="Netherlands Antilles" >Netherlands Antilles</option>
	
      <option value="New Caledonia" >New Caledonia</option>
	
      <option value="New Zealand" >New Zealand</option>
	
      <option value="Nicaragua" >Nicaragua</option>
	
      <option value="Niger" >Niger</option>
	
      <option value="Nigeria" >Nigeria</option>
	
      <option value="Niue" >Niue</option>
	
      <option value="Norfolk Island" >Norfolk Island</option>
	
      <option value="North Korea" >North Korea</option>
	
      <option value="North Sea" >North Sea</option>
	
      <option value="Northern Mariana Islands" >Northern Mariana Islands</option>
	
      <option value="Norway" >Norway</option>
	
      <option value="Oman" >Oman</option>
	
      <option value="Pacific Ocean" >Pacific Ocean</option>
	
      <option value="Pakistan" >Pakistan</option>
	
      <option value="Palau" >Palau</option>
	
      <option value="Palestine " >Palestine </option>
	
      <option value="Palmyra Atoll" >Palmyra Atoll</option>
	
      <option value="Panama" >Panama</option>
	
      <option value="Papua New Guinea" >Papua New Guinea</option>
	
      <option value="Paracel Islands" >Paracel Islands</option>
	
      <option value="Paraguay" >Paraguay</option>
	
      <option value="Peru" >Peru</option>
	
      <option value="Philippines" >Philippines</option>
	
      <option value="Pitcairn Islands" >Pitcairn Islands</option>
	
      <option value="Poland" >Poland</option>
	
      <option value="Portugal" >Portugal</option>
	
      <option value="Puerto Rico" >Puerto Rico</option>
	
      <option value="Qatar" >Qatar</option>
	
      <option value="Republic of the Congo" >Republic of the Congo</option>
	
      <option value="Reunion" >Reunion</option>
	
      <option value="Romania" >Romania</option>
	
      <option value="Ross Sea" >Ross Sea</option>
	
      <option value="Russia" >Russia</option>
	
      <option value="Rwanda" >Rwanda</option>
	
      <option value="Saint Helena" >Saint Helena</option>
	
      <option value="Saint Kitts and Nevis" >Saint Kitts and Nevis</option>
	
      <option value="Saint Lucia" >Saint Lucia</option>
	
      <option value="Saint Pierre and Miquelon" >Saint Pierre and Miquelon</option>
	
      <option value="Saint Vincent and the Grenadines" >Saint Vincent and the Grenadines</option>
	
      <option value="Samoa" >Samoa</option>
	
      <option value="San Marino" >San Marino</option>
	
      <option value="Sao Tome and Principe" >Sao Tome and Principe</option>
	
      <option value="Saudi Arabia" >Saudi Arabia</option>
	
      <option value="Senegal" >Senegal</option>
	
      <option value="Serbia" >Serbia</option>
	
      <option value="Seychelles" >Seychelles</option>
	
      <option value="Sierra Leone" >Sierra Leone</option>
	
      <option value="Singapore" >Singapore</option>
	
      <option value="Slovakia" >Slovakia</option>
	
      <option value="Slovenia" >Slovenia</option>
	
      <option value="Solomon Islands" >Solomon Islands</option>
	
      <option value="Somalia" >Somalia</option>
	
      <option value="South Africa" >South Africa</option>
	
      <option value="South Georgia and the South Sandwich Islands" >South Georgia and the South Sandwich Islands</option>
	
      <option value="South Korea" >South Korea</option>
	
      <option value="Southern Ocean" >Southern Ocean</option>
	
      <option value="Spain" >Spain</option>
	
      <option value="Spratly Islands" >Spratly Islands</option>
	
      <option value="Sri Lanka" >Sri Lanka</option>
	
      <option value="Sudan" >Sudan</option>
	
      <option value="Suriname" >Suriname</option>
	
      <option value="Svalbard" >Svalbard</option>
	
      <option value="Swaziland" >Swaziland</option>
	
      <option value="Sweden" >Sweden</option>
	
      <option value="Switzerland" >Switzerland</option>
	
      <option value="Syria" >Syria</option>
	
      <option value="Taiwan" >Taiwan</option>
	
      <option value="Tajikistan" >Tajikistan</option>
	
      <option value="Tanzania" >Tanzania</option>
	
      <option value="Tasman Sea" >Tasman Sea</option>
	
      <option value="Thailand" >Thailand</option>
	
      <option value="Togo" >Togo</option>
	
      <option value="Tokelau" >Tokelau</option>
	
      <option value="Tonga" >Tonga</option>
	
      <option value="Trinidad and Tobago" >Trinidad and Tobago</option>
	
      <option value="Tromelin Island" >Tromelin Island</option>
	
      <option value="Tunisia" >Tunisia</option>
	
      <option value="Turkey" >Turkey</option>
	
      <option value="Turkmenistan" >Turkmenistan</option>
	
      <option value="Turks and Caicos Islands" >Turks and Caicos Islands</option>
	
      <option value="Tuvalu" >Tuvalu</option>
	
      <option value="Uganda" >Uganda</option>
	
      <option value="Ukraine" >Ukraine</option>
	
      <option value="United Arab Emirates" >United Arab Emirates</option>
	
      <option value="United Kingdom" >United Kingdom</option>
	
      <option value="Uruguay" >Uruguay</option>
	
	
      <option value="Uzbekistan" >Uzbekistan</option>
	
      <option value="Vanuatu" >Vanuatu</option>
	
      <option value="Venezuela" >Venezuela</option>
	
      <option value="Viet Nam" >Viet Nam</option>
	
      <option value="Virgin Islands" >Virgin Islands</option>
	
      <option value="Wake Island" >Wake Island</option>
	
      <option value="Wallis and Futuna" >Wallis and Futuna</option>
	
      <option value="West Bank" >West Bank</option>
	
      <option value="Western Sahara" >Western Sahara</option>
	
      <option value="Yemen" >Yemen</option>
	
      <option value="Zambia" >Zambia</option>
	
      <option value="Zimbabwe" >Zimbabwe</option>
	  
    </select></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="28" colspan="2" class="nStrip">
	<table width="300" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="28" class="ntitle02">Payment  Information </td>
        <td width="20"><img src="images/a-Rconner.jpg" width="20" height="28" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="120" colspan="2" class="nBrdLRB" id="nPadding"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" height="25">Credit card type</td>
        <td width="70%"><select name="CardType">
<option value="">Select...</option>
<option value="MasterCard">MasterCard</option>
<option value="VisaCard">Visa</option>
</select></td>
      </tr>
      <tr>
        <td height="25">Credit card number</td>
        <td><input name="CardNumber" type="text" id="boxSize"  value="<?=$CardNumber?>" size="24" /></td>
      </tr>
      <tr>
        <td height="25">Expiry date</td>
        <td>   <select name="ExpMon">
                  <option value="1" selected>1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
    </select>
                / 
                <input name="ExpYear" size="2" maxlength="2"> 
                  (mm/yy) </td>
      </tr>
      <tr>
        <td height="25">Security code</td>
        <td><input name="CardSecurity" type="text" id="CardSecurity" size="10" /></td>
      </tr>
      <tr>
        <td>Name on credit card</td>
        <td><input name="cardname" type="text" value="<?=$cardname?>" id="boxSize" size="50" OnClick="CheckCardNumber(this.form)" /></td>
      </tr>
    </table></td>
  </tr>
</table>
<br />
<table width="850" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="25" colspan="2" align="center">You must read and agree with the <a id="lightbox03" href="http://www.thebalmainboatcompany.com/terms.php" title="">legal terms and conditions</a> to proceed.</td>
  </tr>
  <tr>
    <td height="25" colspan="2" align="center"> <?php if($ERRORS['chkAgree']) echo $error_open; ?><input name="chkAgree" type="checkbox" id="chkAgree" value="Yes" checked="checked" alt="chkAgree" />

  I have read the instructions and terms and  conditions and agree.
  <?php if($ERRORS['chkAgree']) echo $error_end; ?></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center"> <input type="submit" name="Submit" id="button" value="Submit" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
