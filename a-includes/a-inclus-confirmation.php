<link rel="stylesheet" type="text/css" href="a-css/global.css">
<link rel="stylesheet" type="text/css" href="../a-css/global.css">

<table width="850" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50" height="28" class="nStrip">
	<table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="28" class="ntitle02">Congratulations, your purchase of the BBCo boat kit is complete.</td>
        <td width="20" align="left"><img src="images/a-Rconner.jpg" width="20" height="28" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="120" class="nBrdLRB" id="nPadding">
<p>  Every BBCo boat is special to us and we want to see how you've made the boat your own. Stay in touch and send us your pics and stories.</p>
<p>For international  orders all boats will be dispatched from our overseas suppliers within 14  business days from receipt of payment. After dispatch, shipping time to most  capital cities is usually 7-10 business days.</p>
<p>But please remember  dates are estimates only and we apologize in advance for any delays that may be  experienced.</p>
<p>Please feel free to  contact us with any questions or concerns at <a href="mailto:help@thebalmainboatcompany.com">help@thebalmainboatcompany.com.</a> </p>
<p>When you've built your BBCo boat we want to see it, please follow us on  Facebook and send us your pics. Every BBCo boat is special to us and we want to  see how you've made the boat your own. </p>
<p>Enjoy your boat and happy building. <br />
</p>
	</td>
  </tr>
</table>
<br />
